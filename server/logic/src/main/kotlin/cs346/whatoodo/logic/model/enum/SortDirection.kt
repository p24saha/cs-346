package cs346.whatoodo.logic.model.enum

enum class SortDirection {
    ASC,
    DESC
}