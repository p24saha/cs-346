package cs346.whatoodo.logic.dtos

class UserLoginDTO {
    val email: String = ""
    val password: String = ""
}