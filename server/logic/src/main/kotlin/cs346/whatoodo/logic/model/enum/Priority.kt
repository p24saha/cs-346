package cs346.whatoodo.logic.model.enum

enum class Priority {
    HIGH,
    MEDIUM,
    LOW,
    NONE
}