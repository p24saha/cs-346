package cs346.whatoodo.logic.service

import cs346.whatoodo.logic.dtos.UserLoginDTO
import cs346.whatoodo.logic.dtos.UserRegisterDTO
import cs346.whatoodo.logic.model.enum.ErrorCodes
import cs346.whatoodo.logic.repository.AuthenticationRepository
import cs346.whatoodo.logic.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AuthenticationService(@Autowired private val userRepository: UserRepository,
                            @Autowired private val authenticationRepository: AuthenticationRepository) {
    /**
     * fun passwordLength(password: String): Boolean
    fun passwordRequired(password: String): Boolean
    fun passwordCompare(email: String, password: String): Boolean

    fun emailFormat(email: String): Boolean
    fun emailRequired(email: String): Boolean
    fun emailUnique(email: String): Boolean
    fun findByEmail(email: String): Boolean

    fun usernameLength(username: String): Boolean
    fun findByUsername(username: String): Boolean
    fun usernameFormat(user: String): Boolean
     */

    private fun passwordLength(password: String): Boolean {
        return password.length >= 6
    }

    private fun passwordRequired(password: String): Boolean {
        return password.isNotEmpty()
    }

    private fun emailFormatCheck(email: String): Boolean {
        // format permitted by RFC 5322
        val EMAIl_REGEX = "^[a-zA-Z0-9_!#\$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+\$"
        return EMAIl_REGEX.toRegex().matches(email)
    }

    private fun emailRequired(email: String): Boolean {
        return email.isNotEmpty();
    }

    private fun emailUnique(email: String): Boolean {
        val value = userRepository.findByEmail(email)
            ?: return true // unique
        return false
    }

    private fun usernameLength(username: String): Boolean {
        return username.length in 8..30
    }

    private fun usernameUnique(username: String): Boolean {
        val value = userRepository.findByUsername(username)
            ?: return true // unique
        return false
    }

    private fun usernameRequired(username: String): Boolean {
        return username.isNotEmpty();
    }
    private fun usernameFormat(username: String): Boolean {
        // start with alphabet
        // all other characters can be alphabets, numbers or underscore
        // length 8-30 characters in length
        val USERNAME_REGEX = "^[A-Za-z][A-Za-z0-9_]{7,29}\$"
        return USERNAME_REGEX.toRegex().matches(username)
    }

    fun loginPayloadVerfication(payload: UserLoginDTO): ErrorCodes {
        // email validation
        if (!emailRequired(payload.email)) {
            return ErrorCodes.EMAIL_REQUIRED
        }
        // password validation

        if (!passwordRequired(payload.password)) {
            return ErrorCodes.PASSWORD_REQUIRED
        }
        return ErrorCodes.OK
    }

    fun registrationPayloadVerification(payload: UserRegisterDTO): ErrorCodes {
           /** check if all requried fields are present **/
        if (!usernameRequired(payload.username)) {
            return ErrorCodes.USERNAME_REQUIRED
        }

        if (!emailRequired(payload.email)) {
            return ErrorCodes.EMAIL_REQUIRED
        }

        if (!passwordRequired(payload.password)) {
            return ErrorCodes.PASSWORD_REQUIRED
        }

        /** username verfication **/
        // check if username is unique
        if (!usernameUnique(payload.username)) {
            return ErrorCodes.USERNAME_UNIQUE
        }
        // check is username is within 8-30 characters
        if (!usernameLength(payload.username)) {
            return ErrorCodes.USERNAME_LENGTH
        }
        // check if username has valid format
        if (!usernameFormat(payload.username)) {
            return ErrorCodes.USERNAME_FORMAT
        }

        /** email verfication **/
        if (!emailFormatCheck(payload.email)) {
            return ErrorCodes.EMAIL_INVALID_FORMAT
        }

        if (!emailUnique(payload.email)) {
            return ErrorCodes.EMAIL_UNIQUE
        }

        /** password verfication **/
        if (!passwordLength(payload.password)) {
            return ErrorCodes.PASSWORD_LENGTH
        }


        return ErrorCodes.OK
    }


}