package cs346.whatoodo.logic.dtos

class UserResponseBody (
    val userid: Long,
    val username: String,
    val email: String,
    val password: String,
    val firstname: String?,
    val lastname: String?,
    val occupation: String?,
    var lists: MutableList<Long> = mutableListOf()
)