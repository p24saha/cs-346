package cs346.whatoodo.logic.controller

import cs346.whatoodo.logic.model.TodoItem
import cs346.whatoodo.logic.model.TodoList
import cs346.whatoodo.logic.model.enum.Priority
import cs346.whatoodo.logic.model.enum.SortDirection
import cs346.whatoodo.logic.service.TodoItemService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/todo-item")
class TodoItemController (@Autowired private var itemService: TodoItemService) {
    /**
     * To get all items in a list: userid, listid
     * To get a particular item: userid, listid, itemid
     * To create a new item: userid, listid + item
     * To delete an item: userid, listid, itemid
     * To update an item: userid, listid, itemid + item
     */


    @GetMapping
    fun getItems(): Collection<TodoItem> {
        return itemService.getAllItems()
    }

    @GetMapping("/{id}")
    fun getItem(@PathVariable id: Long): TodoItem {
        return itemService.getItem(id)
    }

    @PostMapping
    fun createItem(@RequestBody item: TodoItem): TodoItem {
        return itemService.addItem(item)
    }

    @DeleteMapping("/{id}")
    fun deleteItem(@PathVariable id: Long): String {
        return itemService.deleteItem(id)
    }

    @PutMapping("/{id}")
    fun updateItem(@RequestBody item: TodoItem, @PathVariable id: Long): TodoItem {
       return itemService.updateItem(item, id)
    }

    @PutMapping("/{item_id}/todo-list/{list_id}")
    fun addListToItem(@PathVariable("item_id") itemId: Long, @PathVariable("list_id") listId: Long): TodoList {
       return itemService.addListToItem(listId, itemId)
    }

    @GetMapping("filter-priority")
    fun filterPriority(@RequestParam("priority") priority: Priority, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        return itemService.filterByPriority(priority, listId)
    }

    @GetMapping("filter-tag")
    fun filterTag(@RequestParam("tag") tag: String, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        return itemService.filterByTag(tag, listId)
    }

    @GetMapping("filter-flagged")
    fun filterFlag(@RequestParam("flagged") isFlagged: Boolean, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        return itemService.filterByFlagged(isFlagged, listId)
    }

    @GetMapping("filter-completed")
    fun filterCompleted(@RequestParam("completed") isCompleted: Boolean, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        return itemService.filterByCompleted(isCompleted, listId)
    }

    @GetMapping("filter-end-date")
    fun filterEndDate(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") date: Date, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        return itemService.filterEndDate(date, listId)
    }

    @GetMapping("filter-start-date")
    fun filterStartDate(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") date: Date, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        return itemService.filterStartDate(date, listId)
    }

    @GetMapping("search")
    fun searchItem(@RequestParam("pattern") pattern: String, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        return itemService.searchItem(pattern, listId)
    }

    @GetMapping("search-all")
    fun searchAllLists(@RequestParam("pattern") pattern: String, @RequestParam("user_id") userId: Long): Collection<TodoItem> {
        return itemService.searchLists(pattern, userId)
    }

    @GetMapping("sort-title")
    fun sortByTitle(@RequestParam("sort") sort: SortDirection, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        var sortedList: Collection<TodoItem> = mutableListOf()
        when(sort) {
            SortDirection.ASC -> sortedList = itemService.sortbyTitleAscending(listId)
            SortDirection.DESC -> sortedList = itemService.sortByTitleDescending(listId)
            else -> sortedList = itemService.sortbyTitleAscending(listId)
        }
        return sortedList
    }

    @GetMapping("sort-tag")
    fun sortByTag(@RequestParam("sort") sort: SortDirection, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        var sortedList: Collection<TodoItem> = mutableListOf()
        when(sort) {
            SortDirection.ASC -> sortedList = itemService.sortByTagAscending(listId)
            SortDirection.DESC -> sortedList = itemService.sortByTagDescending(listId)
            else -> sortedList = itemService.sortByTagAscending(listId)
        }
        return sortedList
    }

    @GetMapping("sort-start-date")
    fun sortByStartDate(@RequestParam("sort") sort: SortDirection, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        var sortedList: Collection<TodoItem> = mutableListOf()
        when(sort) {
            SortDirection.ASC -> sortedList = itemService.sortByStartDateAscending(listId)
            SortDirection.DESC -> sortedList = itemService.sortByStarDateDescending(listId)
            else -> sortedList = itemService.sortByStartDateAscending(listId)
        }
        return sortedList
    }

    @GetMapping("sort-end-date")
    fun sortByEndDate(@RequestParam("sort") sort: SortDirection, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        var sortedList: Collection<TodoItem> = mutableListOf()
        when(sort) {
            SortDirection.ASC -> sortedList = itemService.sortByEndDateAscending(listId)
            SortDirection.DESC -> sortedList = itemService.sortByEndDateDescending(listId)
            else -> sortedList = itemService.sortByEndDateAscending(listId)
        }
        return sortedList
    }

    @GetMapping("sort-priority")
    fun sortByPriority(@RequestParam("sort") sort: SortDirection, @RequestParam("list_id") listId: Long): Collection<TodoItem> {
        var sortedList: Collection<TodoItem> = mutableListOf()
        when(sort) {
            SortDirection.ASC -> sortedList = itemService.sortByPriorityAscending(listId)
            SortDirection.DESC -> sortedList = itemService.sortByPriorityDescending(listId)
            else -> sortedList = itemService.sortByPriorityAscending(listId)
        }
        return sortedList
    }

}