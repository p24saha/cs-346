package cs346.whatoodo.logic.dtos


import com.fasterxml.jackson.annotation.JsonProperty
import cs346.whatoodo.logic.model.enum.ErrorCodes

class Message (
    @JsonProperty("error_code")
    val errorCode: ErrorCodes,
    @JsonProperty("error_message")
    val errorMessage: String)