package cs346.whatoodo.logic.service

import cs346.whatoodo.logic.model.TodoItem
import cs346.whatoodo.logic.model.TodoList
import cs346.whatoodo.logic.model.User
import cs346.whatoodo.logic.model.enum.Priority
import cs346.whatoodo.logic.repository.TodoItemRepository
import cs346.whatoodo.logic.repository.TodoListRepository
import cs346.whatoodo.logic.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.*

@Service
class TodoItemService(@Autowired private var itemRepository: TodoItemRepository,
 @Autowired private var listRepository: TodoListRepository, @Autowired private var userRepository: UserRepository) {

    /**
     * To get all items in a list: userid, listid
     * To get a particular item: userid, listid, itemid
     * To create a new item: userid, listid + item
     * To delete an item: userid, listid, itemid
     * To update an item: userid, listid, itemid + item
     */



    fun getAllItems(): List<TodoItem> {
        return itemRepository.findAll()
    }

    fun getItem(id: Long): TodoItem {
        return itemRepository.findById(id).get()
    }

    fun addItem(item: TodoItem): TodoItem {
        return itemRepository.save(item)
    }

    fun deleteItem(id: Long): String {
        itemRepository.deleteById(id)
        return "Todo Item Deleted"
    }


    fun updateItem (item: TodoItem, id: Long): TodoItem {
        var updateItem: TodoItem = itemRepository.findById(id).get()

        updateItem.itemDescription = item.itemDescription
        updateItem.itemTitle = item.itemTitle
        updateItem.isFlagged = item.isFlagged
        updateItem.priority = item.priority
        updateItem.tag = item.tag
//        updateItem.listId = updateItem.listId
        updateItem.startDate = item.startDate
        updateItem.endDate = item.endDate
        updateItem.isComplete = item.isComplete

        return itemRepository.save(updateItem)
    }

    fun addListToItem(listId: Long, itemId: Long): TodoList {
        /** 1. find the list
         *  2. find the item
         *  3. set list property in item to the given list
         *  4. save the item
         */
        var item: TodoItem = itemRepository.findById(itemId).get()
        var list: TodoList = listRepository.findById(listId).get()

        item.list = list

         itemRepository.save(item)
        return listRepository.findById(listId).get()
    }

    fun filterByPriority(priority: Priority, listId: Long): Collection<TodoItem> {
        // TODO: Add exception handler
        var list = listRepository.findById(listId).get()
        var filteredList: Collection<TodoItem> = itemRepository.filterByPriority(priority, list)

        return filteredList
    }

    fun filterByTag(tag: String, listId: Long): Collection<TodoItem> {
        // TODO: Add exception handler
        var list = listRepository.findById(listId).get()
        var filteredList: Collection<TodoItem> = itemRepository.filterByTag(tag, list)
        return filteredList
    }

    fun filterByFlagged(isFlagged: Boolean, listId: Long): Collection<TodoItem> {
        // TODO: Add exception handler
        var list = listRepository.findById(listId).get()
        var filteredList: Collection<TodoItem> = itemRepository.filterByFlagged(isFlagged, list)
        return filteredList
    }

    fun filterByCompleted(isCompleted: Boolean, listId: Long): Collection<TodoItem> {
        // TODO: Add exception handler
        var list = listRepository.findById(listId).get()
        var filteredList:Collection<TodoItem> = itemRepository.filterByCompleted(isCompleted, list)
        return filteredList
    }

    fun filterEndDate(date: Date, listId: Long): Collection<TodoItem> {
        var list = listRepository.findById(listId).get()
        var filteredList: Collection<TodoItem> = itemRepository.filterByEndDate(date, list)
        return filteredList
    }

    fun filterStartDate(date: Date, listId: Long): Collection<TodoItem> {
        var list = listRepository.findById(listId).get()
        var filteredList: Collection<TodoItem> = itemRepository.filterByStartDate(date, list)
        return filteredList
    }

    fun searchItem(pattern: String, listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var searchList: Collection<TodoItem> = itemRepository.searchList(pattern, list)
        return searchList
    }

    fun searchLists(pattern: String, userId: Long): Collection<TodoItem> {
        var user: User = userRepository.findById(userId).get()
        var lists: Collection<TodoList> = listRepository.getListsForUser(user)
        var searchList= mutableListOf<TodoItem>()
        for (list in lists) {
            searchList.addAll(itemRepository.searchList(pattern, list))
        }
        return searchList
    }

    fun sortbyTitleAscending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByItemTitleAsc(list)
        return sortedList
    }

    fun sortByTitleDescending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByItemTitleDesc(list)
        return sortedList
    }

    fun sortByTagAscending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByTagAsc(list)
        return sortedList
    }

    fun sortByTagDescending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByTagDesc(list)
        return sortedList
    }

    fun sortByStartDateAscending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByStartDateAsc(list)
        return sortedList
    }

    fun sortByStarDateDescending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByStartDateDesc(list)
        return sortedList
    }

    fun sortByEndDateAscending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByEndDateAsc(list)
        return sortedList
    }

    fun sortByEndDateDescending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByEndDateDesc(list)
        return sortedList
    }

    fun sortByPriorityAscending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByPriorityAsc(list)
        return sortedList
    }

    fun sortByPriorityDescending(listId: Long): Collection<TodoItem> {
        var list: TodoList = listRepository.findById(listId).get()
        var sortedList: Collection<TodoItem> = itemRepository.findByListOrderByPriorityDesc(list)
        return sortedList
    }
}