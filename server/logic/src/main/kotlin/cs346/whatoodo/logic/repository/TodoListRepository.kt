package cs346.whatoodo.logic.repository

import cs346.whatoodo.logic.model.TodoList
import cs346.whatoodo.logic.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository


@Repository
interface TodoListRepository: JpaRepository<TodoList, Long> {
    @Query("SELECT l from TodoList l WHERE l.user = :user")
    fun getListsForUser(user: User): Collection<TodoList>
}