package cs346.whatoodo.logic.model

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import cs346.whatoodo.logic.model.enum.Priority
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "todo_item")
class TodoItem () {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("item_id")
    @Column(name = "item_id", nullable = false, unique = true)
    var itemId: Long = 0


    @JsonProperty("item_title")
    @Column(name = "item_title", nullable = false)
    var itemTitle: String = ""

    @JsonProperty("item_description")
    @Column(name = "item_description")
    var itemDescription: String = ""

    @JsonProperty("priority")
    @Column(name = "priority")
    @Enumerated
    var priority: Priority = Priority.NONE

    @JsonProperty("is_flagged")
    @Column(name = "is_flagged")
    var isFlagged: Boolean = false


    @JsonProperty("start_date")
    @Column(name = "start_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    var startDate: Date = Date()

    @JsonProperty("end_date")
    @Column(name = "end_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    var endDate: Date = Date()


    @JsonProperty("tag")
    @Column(name = "tag")
    var tag: String = ""

    @JsonProperty("is_complete")
    @Column(name = "is_complete", nullable = false)
    var isComplete: Boolean = false

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "list_id", nullable = true)
    @JsonBackReference
    var list: TodoList? = null

    constructor(_itemTitle: String, _itemDescription: String = "", _priority: Priority = Priority.NONE, _isFlagged: Boolean = false,
                _startDate: Date = Date(), _endDate: Date = Date(), _tag: String = "", _isComplete: Boolean = false) : this() {
        this.itemTitle = _itemTitle
        this.itemDescription = _itemDescription
        this.priority = _priority
        this.isFlagged = _isFlagged
        this.startDate = _startDate
        this.endDate = _endDate
        this.tag = _tag
        this.isComplete = _isComplete
    }

}

