package cs346.whatoodo.logic.repository

import cs346.whatoodo.logic.model.User
import cs346.whatoodo.logic.model.enum.ErrorCodes
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AuthenticationRepository: JpaRepository<User, Long> {
}