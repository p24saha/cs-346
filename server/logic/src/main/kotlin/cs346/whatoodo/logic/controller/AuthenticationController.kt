package cs346.whatoodo.logic.controller

import cs346.whatoodo.logic.dtos.Message
import cs346.whatoodo.logic.dtos.UserLoginDTO
import cs346.whatoodo.logic.dtos.UserRegisterDTO
import cs346.whatoodo.logic.dtos.UserResponseBody
import cs346.whatoodo.logic.model.User
import cs346.whatoodo.logic.model.enum.ErrorCodes
import cs346.whatoodo.logic.service.AuthenticationService
import cs346.whatoodo.logic.service.UserService
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.impl.TextCodec
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties.Jwt
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CookieValue
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.Exception
import java.util.*
import javax.crypto.SecretKey
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("/user-auth")
class AuthenticationController(@Autowired private var userService: UserService,
                               @Autowired private val authenticationService: AuthenticationService) {

    @PostMapping("/register")
    fun register(@RequestBody body: UserRegisterDTO): ResponseEntity<Any> {
        /** registration payload verification **/
        val verification: ErrorCodes = authenticationService.registrationPayloadVerification(body)
        if (verification != ErrorCodes.OK) {
            return ResponseEntity.badRequest().body(Message(verification, verification.value))
        }
        val user = User()
        user.username = body.username
        user.email = body.email
        user.password = body.password
        if (body.firstname.isNullOrEmpty()) {
            user.firstname = body.username
        } else {
            user.firstname = body.firstname
        }

        user.lastname = body.lastname
        user.occupation = body.occupation
        this.userService.addUser(user)
        return ResponseEntity.ok(Message(ErrorCodes.REGISTRATION_SUCCESS, ErrorCodes.REGISTRATION_SUCCESS.value))
    }

    @PostMapping("/login")
    fun login(@RequestBody body: UserLoginDTO, response: HttpServletResponse): ResponseEntity<Any> {

        /** login payload verification **/
        val verification: ErrorCodes = authenticationService.loginPayloadVerfication(body)
        if (verification != ErrorCodes.OK) {
            return ResponseEntity.badRequest().body(Message(verification, verification.value))
        }
        val user = this.userService.emailAuthentication(body.email) ?:
                    return ResponseEntity.badRequest()
                        .body(Message(ErrorCodes.EMAIL_INCORRECT, ErrorCodes.EMAIL_INCORRECT.value))

        if (!user.comparePassword(body.password)) {
            return ResponseEntity.badRequest()
                .body(Message(ErrorCodes.PASSWORD_INCORRECT, ErrorCodes.PASSWORD_INCORRECT.value))
        }

        val issuer = user.userid.toString()

        val jwt = Jwts.builder().setIssuer(issuer)
            .setExpiration(Date(System.currentTimeMillis() + 60 * 24 * 1000)) // one day
            .signWith(SignatureAlgorithm.HS512, "secret").compact()

        val cookie = Cookie("jwt", jwt)
        // cookie.isHttpOnly = true

        response.addCookie(cookie)

        return ResponseEntity.ok(Message(ErrorCodes.LOGIN_SUCCESS, ErrorCodes.LOGIN_SUCCESS.value))
    }

    @GetMapping("/user")
    fun user(request: HttpServletRequest): ResponseEntity<Any> {
        try {
            var jwt = request.getHeader("Cookie").subSequence(4, request.getHeader("Cookie").length)
            println(jwt)
            if (jwt == null) {
                val userResponse: UserResponseBody = UserResponseBody(0, "", "", "", "", "", "")
                return ResponseEntity.badRequest().body(userResponse)
            }
            val body = Jwts.parser().setSigningKey("secret").parseClaimsJws(jwt as String?).body

            val user: User = this.userService.getUser(body.issuer.toLong())
            val userResponse: UserResponseBody = UserResponseBody(user.userid, user.username, user.email, user.password,
                                                user.firstname, user.lastname, user.occupation)
            for (list in user.lists) {
                userResponse.lists.add(list.listId)
            }
//
            return ResponseEntity.ok().body(userResponse)

        } catch(e: Exception) {
            return ResponseEntity.status(401)
                .body(Message(ErrorCodes.USER_AUTHENTICATED_FAILURE,
                    ErrorCodes.USER_AUTHENTICATED_FAILURE.value))
        }

    }

    @PostMapping("/logout")
    fun logout(response: HttpServletResponse): ResponseEntity<Any> {
        val cookie = Cookie("jwt", "")
        cookie.maxAge = 0
        response.addCookie(cookie)
        return ResponseEntity.ok(Message(ErrorCodes.LOGOUT_SUCCESS, ErrorCodes.LOGOUT_SUCCESS.value))
    }

}