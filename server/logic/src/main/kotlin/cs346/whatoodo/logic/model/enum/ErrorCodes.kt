package cs346.whatoodo.logic.model.enum

enum class ErrorCodes(val value: String) {
    PASSWORD_REQUIRED("Password is required."),
    PASSWORD_LENGTH("Password must be at least 6 characters in length."),
    EMAIL_REQUIRED("Email address is required"),
    EMAIL_UNIQUE("An account with this email address already exists. Please use another email address"),
    EMAIL_INVALID_FORMAT("Invalid email address format. Please enter a valid email address"),
    USERNAME_LENGTH("Username must be at least 8 - 30 characters in length."),
    USERNAME_UNIQUE("An account with this username already exists. Please choose another username."),
    USERNAME_REQUIRED("Username is required"),
    USERNAME_FORMAT("Username format is invalid."),
    USER_NOT_FOUND("user not found"),
    LOGIN_SUCCESS("User logged in successfully"),
    USER_AUTHENTICATED_SUCCESS("User authenticated successfully."),
    USER_AUTHENTICATED_FAILURE("User authentication failed."),
    EMAIL_INCORRECT("Invalid email address. No account with this email address exists."),
    PASSWORD_INCORRECT("Incorrect password. Please try again."),
    OK("OK"),
    LOGOUT_SUCCESS("Logged out successfully."),
    REGISTRATION_SUCCESS("User successfully registered.")
}