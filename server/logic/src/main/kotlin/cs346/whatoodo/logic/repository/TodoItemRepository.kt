package cs346.whatoodo.logic.repository

import cs346.whatoodo.logic.model.TodoItem
import cs346.whatoodo.logic.model.TodoList
import cs346.whatoodo.logic.model.enum.Priority
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface TodoItemRepository: JpaRepository<TodoItem, Long> {
    /** filtering **/
    @Query("SELECT i FROM TodoItem i WHERE i.priority = :priority AND i.list = :list")
    fun filterByPriority(priority: Priority, list: TodoList): Collection<TodoItem>

    @Query("SELECT i FROM TodoItem i WHERE LOWER(i.tag) LIKE LOWER(CONCAT('%', :tag, '%')) " +
            "AND i.list = :list")
    fun filterByTag(tag: String, list: TodoList): Collection<TodoItem>

    @Query("SELECT i FROM TodoItem i WHERE i.isFlagged = :isFlagged AND i.list = :list")
    fun filterByFlagged(isFlagged: Boolean, list: TodoList): Collection<TodoItem>

    @Query("SELECT i FROM TodoItem i WHERE i.isComplete = :isCompleted AND i.list = :list")
    fun filterByCompleted(isCompleted: Boolean, list: TodoList): Collection<TodoItem>

    @Query("SELECT i FROM TodoItem i WHERE CAST(i.endDate AS date) = :date AND i.list = :list")
    fun filterByEndDate(date: Date, list: TodoList): Collection<TodoItem>

    @Query("SELECT i FROM TodoItem i WHERE CAST(i.startDate AS date) = :date AND i.list = :list")
    fun filterByStartDate(date: Date, list: TodoList): Collection<TodoItem>

    /** search **/
    @Query("SELECT i FROM TodoItem i WHERE i.list = :list AND " +
            "(LOWER(REPLACE(i.itemTitle, ' ', '')) LIKE CONCAT('%', REPLACE(:pattern, ' ', ''), '%') OR " +
            "LOWER(REPLACE(i.itemDescription, ' ', '')) LIKE CONCAT('%', REPLACE(:pattern, ' ', ''), '%'))")
    fun searchList(pattern: String, list: TodoList): Collection<TodoItem>

    /** sorting **/

    /** By Title **/
    fun findByListOrderByItemTitleAsc(list: TodoList): Collection<TodoItem>
    fun findByListOrderByItemTitleDesc(list: TodoList): Collection<TodoItem>

    /** By Tag **/
    fun findByListOrderByTagAsc(list: TodoList): Collection<TodoItem>
    fun findByListOrderByTagDesc(list: TodoList): Collection<TodoItem>

    /** By Start Date **/
    fun findByListOrderByStartDateAsc(list: TodoList): Collection<TodoItem>
    fun findByListOrderByStartDateDesc(list: TodoList): Collection<TodoItem>

    /** By End Date **/
    fun findByListOrderByEndDateAsc(list: TodoList): Collection<TodoItem>
    fun findByListOrderByEndDateDesc(list: TodoList): Collection<TodoItem>

    /** By Priority **/
    fun findByListOrderByPriorityAsc(list: TodoList): Collection<TodoItem>
    fun findByListOrderByPriorityDesc(list: TodoList): Collection<TodoItem>
}