package cs346.whatoodo.logic.model

import com.fasterxml.jackson.annotation.JsonManagedReference
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import javax.persistence.*


@Entity
@Table(name = "user")
class User () {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    @JsonProperty("userid")
    var userid: Long = 0

    @Column(unique = true, nullable = false)
    @JsonProperty("username")
    var username: String = ""

    @Column(unique = true, nullable = false)
    @JsonProperty("email")
    var email: String = ""

    @Column(nullable = false)
    @JsonProperty("password")
    var password: String = ""
        get() = field
        set(value) {
            val passwordEncoder = BCryptPasswordEncoder()
            field = passwordEncoder.encode(value)
        }

    @JsonProperty("firstname")
    var firstname: String? = ""

    @JsonProperty("lastname")
    var lastname: String? = ""

    @JsonProperty("occupation")
    var occupation: String? = ""

    /** user can have multiple lists **/
    @JsonManagedReference
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = [(CascadeType.ALL)])
    @JsonProperty("todo_lists")
    var lists: MutableList<TodoList> = mutableListOf<TodoList>()

    constructor(_username: String, _email: String, _password: String, _firstname: String?, _lastname: String?, _occupation: String?): this() {
        this.username = _username
        this.email = _email
        this.password = _password
        this.firstname = _firstname
        this.lastname = _lastname
        this.occupation = _occupation
    }

    constructor(_username: String, _email: String, _password: String): this() {
        this.username = _username
        this.email = _email
        this.password = _password
        this.firstname = _username
        this.lastname = ""
        this.occupation = ""
    }

    fun comparePassword(password: String): Boolean {
        return BCryptPasswordEncoder().matches(password, this.password)
    }

}



