package cs346.whatoodo.logic.service

import cs346.whatoodo.logic.model.TodoItem
import cs346.whatoodo.logic.model.TodoList
import cs346.whatoodo.logic.model.User
import cs346.whatoodo.logic.repository.TodoItemRepository
import cs346.whatoodo.logic.repository.TodoListRepository
import cs346.whatoodo.logic.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.PathVariable

@Service
class TodoListService(@Autowired private var listRepository: TodoListRepository,
                      @Autowired private var userRepository: UserRepository) {

   fun getLists(): List<TodoList> {
       return listRepository.findAll()
   }

   fun getListById(id: Long): TodoList {
       return listRepository.findById(id).get()
   }

    fun getListsByUserId(userId: Long): Collection<TodoList> {
        val user = userRepository.findById(userId).get()
        return listRepository.getListsForUser(user)
    }

    fun createList(list: TodoList): TodoList {
        return listRepository.save(list)
    }

    fun deleteList(id: Long): String {
        listRepository.deleteById(id)
        return "List Deleted"
    }

    fun updateList(list: TodoList, id: Long): TodoList {
        var updateList: TodoList = listRepository.findById(id).get()
        updateList.listName = list.listName
        updateList.category = list.category
        updateList.isFavourite = list.isFavourite
        listRepository.save(updateList)

        return listRepository.findById(id).get()
    }


    fun addUsertoList(userId: Long, listId: Long): User {
        var list: TodoList = listRepository.findById(listId).get()
        var user: User = userRepository.findById(userId).get()

        list.user = user
        listRepository.save(list)
        return userRepository.findById(userId).get()
    }




}