package cs346.whatoodo.logic.dtos

class UserRegisterDTO {
    val username: String = ""
    val email: String = ""
    val password: String = ""
    val firstname: String? = ""
    val lastname: String? = ""
    val occupation: String? = ""
}