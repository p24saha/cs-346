package cs346.whatoodo.logic.model

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonManagedReference
import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*

@Entity
@Table(name = "todo_list")
class TodoList () {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("list_id")
    @Column(name="list_id", nullable = false, unique = true)
    var listId: Long = 0

    @JsonProperty("list_name")
    @Column(name = "list_name", nullable = false)
    var listName: String = ""

    @JsonProperty("category")
    @Column(name = "category")
    var category: String = ""

    @JsonProperty("is_favourite")
    @Column(name = "is_favourite", nullable = false)
    var isFavourite: Boolean = false

//    @Column(name="userid", nullable = false)
//    @JsonProperty("user_id")
//    var userId: Long



    /** list can have multiple items **/
    @JsonManagedReference
    @OneToMany(mappedBy = "list", fetch = FetchType.LAZY, cascade = [(CascadeType.ALL)])
    @JsonProperty("todo_items")
    var items: MutableList<TodoItem> = mutableListOf<TodoItem>()

    /** list can have one user **/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid", nullable = true)
    @JsonBackReference
    var user: User? = null

    constructor(_listName: String, _category: String = "", _isFavourite: Boolean = false): this() {
        this.listName = _listName
        this.category = _category
        this.isFavourite = _isFavourite
    }
}

