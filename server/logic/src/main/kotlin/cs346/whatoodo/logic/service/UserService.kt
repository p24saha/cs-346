package cs346.whatoodo.logic.service

import cs346.whatoodo.logic.model.User
import cs346.whatoodo.logic.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService(@Autowired private var userRepository: UserRepository) {

    fun getAllUsers(): Collection<User> {
        return userRepository.findAll()
    }

    fun getUser(id: Long): User {
        return userRepository.findById(id).get()
    }

    fun deleteUser(id: Long): String {
        userRepository.deleteById(id)
        return "User Deleted"
    }

    fun addUser(user: User): User {

        return  userRepository.save(user)
    }

    fun updateUser(id: Long, user: User): User {
        var updateUser: User = userRepository.findById(id).get()

        updateUser.username = user.username
        updateUser.email = user.email
        updateUser.password = user.password
        updateUser.firstname = user.firstname
        updateUser.lastname = user.lastname
        updateUser.occupation = user.occupation


        return userRepository.save(updateUser)
    }

    fun emailAuthentication(email: String): User? {
        return this.userRepository.findByEmail(email)
    }

    fun usernameAuthentication(username: String): User?{
        return this.userRepository.findByUsername(username)
    }
}