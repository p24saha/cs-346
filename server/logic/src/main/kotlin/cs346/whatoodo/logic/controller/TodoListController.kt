package cs346.whatoodo.logic.controller

import cs346.whatoodo.logic.model.TodoList
import cs346.whatoodo.logic.model.User
import cs346.whatoodo.logic.service.TodoListService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.LongSummaryStatistics

@RestController
@RequestMapping("/todo-list")
class TodoListController(@Autowired private var listService: TodoListService) {


    @GetMapping
    fun getLists(): Collection<TodoList> {
        return listService.getLists()
    }

    @GetMapping("/{id}")
    fun getList(@PathVariable id: Long): TodoList {
        return listService.getListById(id)
    }

    @GetMapping("/all-lists/{userid}")
    fun getListsByUser(@PathVariable("userid") userId: Long): Collection<TodoList> {
        return listService.getListsByUserId(userId)
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createList(@RequestBody list: TodoList): TodoList {
        return listService.createList(list)
    }


    @DeleteMapping("/{id}")
    fun deleteList(@PathVariable id: Long): String {
        return listService.deleteList(id)
    }

    @PutMapping("{id}")
    fun updateList(@PathVariable id: Long, @RequestBody list: TodoList): TodoList {
        return listService.updateList(list, id)
    }

    @PutMapping("/{list_id}/user/{user_id}")
    fun addUserToList(@PathVariable("list_id") listId: Long, @PathVariable("user_id") userId: Long): User {
        return listService.addUsertoList(userId, listId)
    }

}