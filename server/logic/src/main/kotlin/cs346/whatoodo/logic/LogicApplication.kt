package cs346.whatoodo.logic

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.r2dbc.R2dbcAutoConfiguration
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import javax.sql.DataSource

@SpringBootApplication(exclude = [R2dbcAutoConfiguration::class])
class LogicApplication

fun main(args: Array<String>) {
    runApplication<LogicApplication>(*args)
}

/**
 * User has many lists
 * Lists has many todo-items
 *
 * Todo-item can belong to one list
 * List can belong to one user
 *
 * User --> Many
 *
 * List --> Item
 * Set of Items --> One List
 */


@Configuration
class DatasourceConfig {
    @Bean
    fun datasource(): DataSource {
        return DataSourceBuilder.create()
            .driverClassName("com.mysql.cj.jdbc.Driver")
            .url("jdbc:mysql://localhost:3306/whatoodo")
            .username("root")
            .password("samika2004")
            .build()
    }
}

@Configuration
class SecurityConfiguration : WebSecurityConfigurerAdapter() {
    @Throws(Exception::class)
    override fun configure(httpSecurity: HttpSecurity) {
        httpSecurity.authorizeRequests().antMatchers("/").permitAll()
        httpSecurity.csrf().disable()
    }
}
