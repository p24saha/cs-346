# WhatTooDo? -  A CS-346 TODO-list app
This repository contains a todo-list application built in Kotlin, with JavaFX. The server runs with Spring boot. There is also a console application for those who prefer a text-based experience.

## Wiki
This wiki contains our main landing page for the project. It goes into much further depth than this README, so please check it out: [landing page for our project](https://git.uwaterloo.ca/p24saha/cs-346/-/wikis/home)
Some things available on our landing page:
- Sprint releases
- Installers
- API Documentation (Spring Server)
- User guide

## Issues and Milestones
We used [issues](https://git.uwaterloo.ca/p24saha/cs-346/-/issues) to track our work done. Think of these issues kind of like a Jira ticket. Issues include features, bugs, general sprint goals, research, refactoring, or anything else we need to do for a release.
