package com.whatoodo.desktop.Context

import com.google.gson.Gson
import model.TodoItem
import payload.TodoItemPayload
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class TodoItemContext {
    var server_address: String = "http://localhost:8000"
    fun getItems(){

    }

    fun getItemById(){

    }

    fun addItem(payload: TodoItemPayload): TodoItem {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-item"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        println(response.body())
        return gson.fromJson(response.body(), TodoItem::class.java)
    }

    fun removeItem(listItem: Int) : HttpResponse<String>? {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-item/$listItem"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).DELETE().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())

        return response
    }

    fun editItem(itemId: Long, payload: TodoItemPayload): TodoItem {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-item/$itemId"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .PUT(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), TodoItem::class.java)
    }

    fun addItemToList(itemId: Long, payload: TodoItemPayload): TodoItem {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)
        val client = HttpClient.newBuilder().build()
        // note: my listId is 1, may be different for you
        val endpoint = "$server_address/todo-item/$itemId/todo-list/1"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .PUT(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), TodoItem::class.java)
    }


}