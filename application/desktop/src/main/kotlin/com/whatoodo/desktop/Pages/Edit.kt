package com.whatoodo.desktop.Pages



import model.IObserver
import model.Presenter
import com.whatoodo.desktop.Components.ToolBarUI
import com.whatoodo.desktop.Context.TodoItemContext
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import javafx.scene.paint.Color
import java.net.URI
import java.net.http.HttpClient
import java.time.LocalDate
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import kotlinx.serialization.json.Json
import kotlinx.serialization.encodeToString
import model.TodoItem
import payload.TodoItemPayload
import java.time.format.DateTimeFormatter


class Edit(itemId: Long, name: String, group: String, shared: String, startDate: String, dueDate: String, priority: Int, starred: Boolean, isComplete: Boolean, private val presenter: Presenter) : Scene(
    BorderPane(),
    800.0,
    600.0


),

    // PRE-FILLED FIELDS

    // need category and tag

    IObserver {
    var welcome = Label("Edit Task")

    private val itemId = itemId
    private val currentName = name
    private val currentCategory = group
    private val currentTag = shared
    private val currentPriority = priority
    private val currentStartDate = startDate
    private val currentDueDate = dueDate


    private fun createLayout() {
        // setup individual elements
        val tool = ToolBarUI(presenter)




        var addPage = VBox()
        var title = Label("Edit Existing Task")
        title.setStyle("-fx-font-weight: bold")
        title.setStyle("-fx-font-size: 20")


        var container = VBox(title)
        container.background = Background(BackgroundFill(Color.WHITE, CornerRadii(20.0), Insets.EMPTY))
        container.prefHeight = 500.0
        container.padding = Insets(10.0, 20.0, 10.0, 20.0)
        container.alignment = Pos.TOP_LEFT
        container.spacing = 30.0

        // EXISTING PARAMETERS

        // ADD NAME

        var nameText = TextField(currentName)
        HBox.setHgrow(nameText, Priority.ALWAYS)
        nameText.prefHeight = 15.0
        val nameBox = HBox(Label("Item Name:"), nameText)
        nameBox.spacing = 10.0
        nameBox.alignment = Pos.CENTER_LEFT
        container.children.add(nameBox)


        // ADD WHICH LIST


        var filterOptions = ComboBox<String>()
        filterOptions.items.addAll("School", "Gym", "Hobbies", "Work")
        filterOptions.value = "School"

        var whichList = HBox(Label("Add to: "), filterOptions)
        whichList.spacing = 15.0
        whichList.alignment = Pos.CENTER_LEFT
        container.children.add(whichList)


        // ADD CATEGORY
        var categoryText = TextField(currentCategory)

        HBox.setHgrow(categoryText, Priority.ALWAYS)
        categoryText.prefHeight = 15.0

        var categoryBox = HBox(Label("Category: "), categoryText)
        categoryBox.spacing = 15.0
        categoryBox.alignment = Pos.CENTER_LEFT
        container.children.add(categoryBox)

        // ADD TAGS
        var tagText = TextField(currentTag)
        HBox.setHgrow(tagText, Priority.ALWAYS)
        tagText.prefHeight = 15.0
        var tagBox = HBox(Label("Tags: "), tagText)
        tagBox.spacing = 10.0
        tagBox.alignment = Pos.CENTER_LEFT
        container.children.add(tagBox)

        // ADD SHARED USERS

        /*
        var userText = TextField()
        HBox.setHgrow(userText, Priority.ALWAYS)
        userText.prefWidth = 300.0
        userText.prefHeight = 15.0
        var userBox = HBox(Label("Share With: "), userText)
        userBox.spacing = 10.0
        userBox.alignment = Pos.CENTER_LEFT
        container.children.add(userBox) */

        // ADD PRIORITY

        val priorityBox = HBox()
        val priLabel = Label("Priority:")
        var priGroup = ToggleGroup()
        var low = RadioButton("Low")
        var med = RadioButton("Medium")
        var hi = RadioButton("High")
        low.toggleGroup = priGroup
        med.toggleGroup = priGroup

        hi.toggleGroup = priGroup
        priorityBox.children.addAll(priLabel, low, med, hi)
        priorityBox.spacing = 10.0
        container.children.add(priorityBox)

        if (currentPriority == 2) {
            hi.isSelected = true
        } else if (currentPriority == 1) {
            med.isSelected = true
        } else {
            low.isSelected = true
        }

        // ADD START DATE
        var startdate = DatePicker(LocalDate.parse(currentStartDate))
        var startdateBox = HBox(Label("Start Date: "), startdate)
        startdateBox.spacing = 10.0
        startdateBox.alignment = Pos.CENTER_LEFT
        container.children.add(startdateBox)

        // ADD DATE
        var date = DatePicker(LocalDate.parse(currentDueDate))
        var dateBox = HBox(Label("Due Date: "), date)
        dateBox.spacing = 10.0
        dateBox.alignment = Pos.CENTER_LEFT
        container.children.add(dateBox)



        // SUBMIT BUTTON


        var submitBtn = Button("Save Item")
        submitBtn.onMouseClicked = EventHandler { mouseEvent: MouseEvent? ->
            run {


                val SERVER_ADDRESS = "http://127.0.0.1:8080/lists/1/1"

                // get priority
                var priority = "MEDIUM"
                priority = if (med.isSelected) {
                    "MEDIUM"
                } else if (low.isSelected) {
                    "LOW"
                } else {
                    "HIGH"
                }

                var format: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd 05:mm:ss")

                var todoitemcontext = TodoItemContext()

                var payload: TodoItemPayload = TodoItemPayload(
                    nameText.text,
                    categoryText.text,
                    priority,
                    false,
                    startdate.value.atStartOfDay().format(format),
                    date.value.atStartOfDay().format(format),
                    tagText.text,
                    false
                )

                todoitemcontext.editItem(itemId, payload)


                // get to check whether our post request worked


                //val response = URL(SERVER_ADDRESS).readText();
                //print(Json.parseToJsonElement(response).jsonArray[0])
                presenter.advance()

            }
        }
        container.children.add(submitBtn)

        addPage.background = Background(BackgroundFill(Color.web("#F0ECF9"), CornerRadii.EMPTY, Insets.EMPTY))
        addPage.alignment = Pos.TOP_CENTER
        addPage.padding = Insets(30.0, 30.0, 30.0, 30.0)

        addPage.children.add(container)

        //addScroll.content = addPage
        val top = VBox(tool, addPage)



        // add everything to the root
        val root = this.root as BorderPane
        root.style = "-fx-background-color: #F0ECF9"
        root.center = top
    }

    // updates are driven by the presenter, not the model
    override fun update() {

    }

    init {
        createLayout()
        update()
    }


    fun post(todoItem: TodoItem): String {
        val SERVER_ADDRESS = "http://127.0.0.1:8080/lists"
        val string = Json.encodeToString(todoItem)

        //print(string)

        val client = HttpClient.newBuilder().build();
        val request = HttpRequest.newBuilder()
            .uri(URI.create(SERVER_ADDRESS))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(string))
            .build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body()

        /*
        Sample JSON to test in postman :)
        {
        "listId": 2,
        "itemID": 10,
        "itemTitle": "Counter-strike global fofensive",
        "itemDescription": "",
        "endDate": "2022-11-30",
        "startDate": "2022-11-30",
        "tag": "Hello World",
        "isFlagged": false,
        "priority": "HIGH"
        }
         */
    }
}