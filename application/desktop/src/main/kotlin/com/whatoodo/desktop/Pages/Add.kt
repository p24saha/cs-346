package com.whatoodo.desktop.Pages


import model.IObserver
import model.Presenter
import com.whatoodo.desktop.Components.ToolBarUI
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import javafx.scene.paint.Color
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import com.whatoodo.desktop.Context.TodoItemContext

import payload.TodoItemPayload
import java.time.format.DateTimeFormatter


class Add(private val presenter: Presenter) : Scene(
        BorderPane(),
        presenter.width,
        presenter.height
),
    IObserver {
    var welcome = Label("Add tasks:")

    private fun createLayout() {
        // setup individual elements
        val tool = ToolBarUI(presenter)




        var addPage = VBox()
        var title = Label("Add a To-do Item")
        title.setStyle("-fx-font-weight: bold")
        title.setStyle("-fx-font-size: 20")


        var container = VBox(title)
        container.background = Background(BackgroundFill(Color.WHITE, CornerRadii(20.0), Insets.EMPTY))
        container.prefHeight = 500.0
        container.padding = Insets(10.0, 20.0, 10.0, 20.0)
        container.alignment = Pos.TOP_LEFT
        container.spacing = 30.0

        // ADD NAME

        var nameText = TextField()
        HBox.setHgrow(nameText, Priority.ALWAYS)
        nameText.prefHeight = 15.0
        val nameBox = HBox(Label("Item Name:"), nameText)
        nameBox.spacing = 10.0
        nameBox.alignment = Pos.CENTER_LEFT
        container.children.add(nameBox)


        // ADD WHICH LIST


        var filterOptions = ComboBox<String>()
        filterOptions.items.addAll("School", "Gym", "Hobbies", "Work")

        var whichList = HBox(Label("Add to: "), filterOptions)
        whichList.spacing = 15.0
        whichList.alignment = Pos.CENTER_LEFT
        container.children.add(whichList)


        // ADD CATEGORY
        var categoryText = TextField()

        HBox.setHgrow(categoryText, Priority.ALWAYS)
        categoryText.prefHeight = 15.0

        var categoryBox = HBox(Label("Category: "), categoryText)
        categoryBox.spacing = 15.0
        categoryBox.alignment = Pos.CENTER_LEFT
        container.children.add(categoryBox)

        // ADD TAGS
        var tagText = TextField()
        HBox.setHgrow(tagText, Priority.ALWAYS)
        tagText.prefHeight = 15.0
        var tagBox = HBox(Label("Tags: "), tagText)
        tagBox.spacing = 10.0
        tagBox.alignment = Pos.CENTER_LEFT
        container.children.add(tagBox)

        // ADD SHARED USERS

        /*
        var userText = TextField()
        HBox.setHgrow(userText, Priority.ALWAYS)
        userText.prefWidth = 300.0
        userText.prefHeight = 15.0
        var userBox = HBox(Label("Share With: "), userText)
        userBox.spacing = 10.0
        userBox.alignment = Pos.CENTER_LEFT
        container.children.add(userBox) */

        // ADD PRIORITY

        val priorityBox = HBox()
        val priLabel = Label("Priority:")
        var priGroup = ToggleGroup()
        var low = RadioButton("Low")
        var med = RadioButton("Medium")
        var hi = RadioButton("High")
        low.toggleGroup = priGroup
        med.toggleGroup = priGroup

        hi.toggleGroup = priGroup
        priorityBox.children.addAll(priLabel, low, med, hi)
        priorityBox.spacing = 10.0
        container.children.add(priorityBox)

        // ADD START DATE
        var startdate = DatePicker()
        var startdateBox = HBox(Label("Start Date: "), startdate)
        startdateBox.spacing = 10.0
        startdateBox.alignment = Pos.CENTER_LEFT
        container.children.add(startdateBox)

        // ADD DATE
        var date = DatePicker()
        var dateBox = HBox(Label("Due Date: "), date)
        dateBox.spacing = 10.0
        dateBox.alignment = Pos.CENTER_LEFT
        container.children.add(dateBox)



        // SUBMIT BUTTON


        var submitBtn = Button("Create Item")
        submitBtn.onMouseClicked = EventHandler { mouseEvent: MouseEvent? ->
            run {


                val SERVER_ADDRESS = "http://127.0.0.1:8080/lists/1/1"

                // get priority
                var priority = "MEDIUM"
                priority = if (med.isSelected) {
                    "MEDIUM"
                } else if (low.isSelected) {
                    "LOW"
                } else {
                    "HIGH"
                }

                var todoitemcontext = TodoItemContext()

                var format: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")



                var payload: TodoItemPayload = TodoItemPayload(
                    nameText.text,
                    categoryText.text,
                    priority,
                    false,
                    startdate.value.atStartOfDay().format(format),
                    date.value.atStartOfDay().format(format),
                    tagText.text,
                    false,

                )

                var newItem = todoitemcontext.addItem(payload)

                // now, we attach the new item to the todolist
                todoitemcontext.addItemToList(newItem.item_id, payload)

                presenter.advance()

            }
        }
        container.children.add(submitBtn)
        if (presenter.settings.darkmode) {
            addPage.background  = Background(BackgroundFill(Color.web("#1A1A33"), CornerRadii.EMPTY, Insets.EMPTY))
        } else {
            addPage.background  = Background(BackgroundFill(Color.web("#F0ECF9"), CornerRadii.EMPTY, Insets.EMPTY))
        }

        addPage.alignment = Pos.TOP_CENTER
        addPage.padding = Insets(30.0, 30.0, 30.0, 30.0)

        addPage.children.add(container)

        //addScroll.content = addPage
        val top = VBox(tool, addPage)



        // add everything to the root
        val root = this.root as BorderPane
        if (presenter.darkmode) {
            root.style = "-fx-background-color: #1A1A33"
        } else {
            root.style = "-fx-background-color: #F0ECF9"
        }
        root.center = top
    }


    init {
        createLayout()
        update()
    }

    override fun update() {

    }


    fun putListToItem(listId: Int, itemId: Int): String {
        val SERVER_ADDRESS = "http://127.0.0.1:8000/todo-item/${itemId}/todo-list/${listId}"

        val client = HttpClient.newBuilder().build();
        val request = HttpRequest.newBuilder()
                .uri(URI.create(SERVER_ADDRESS))
                .header("Content-Type", "application/json")
                .PUT(HttpRequest.BodyPublishers.ofString("{\n" +
                        "    \"username\": \"priya\",\n" +
                        "    \"email\": \"priya@gmail.com\",\n" +
                        "    \"password\": \"priya\",\n" +
                        "    \"firstname\": \"priya\",\n" +
                        "    \"lastname\": \"saha\",\n" +
                        "    \"occupation\": \"student\"\n" +
                        "}"))
                .build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body()
    }
}
