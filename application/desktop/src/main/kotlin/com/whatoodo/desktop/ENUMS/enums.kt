package com.whatoodo.desktop.ENUMS

enum class Priority {
    HIGH,
    MEDIUM,
    LOW,
    NONE
}

enum class ActionType {
    ADD,
    DELETE,
    CUT_PASTE,
    COPY_PASTE,
}