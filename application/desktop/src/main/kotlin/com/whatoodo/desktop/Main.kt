package com.whatoodo.desktop

import javafx.application.Application



import javafx.stage.Stage
import model.Model
import model.Presenter

class Main : Application() {
    override fun start(stage: Stage) {
        val model = Model()

        val presenter = Presenter(stage, model)
        presenter.start()
    }
}
fun main() {
   Application.launch(Main::class.java)
}