package com.whatoodo.desktop.Pages

import com.google.gson.Gson
import com.whatoodo.desktop.*
import com.whatoodo.desktop.Components.Heading
import com.whatoodo.desktop.Components.ItemBox
import com.whatoodo.desktop.Components.ItemList
import com.whatoodo.desktop.Components.ToolBarUI
import com.whatoodo.desktop.ENUMS.ActionType
import com.whatoodo.desktop.HelperFunctions.APIRequests
import com.whatoodo.desktop.TodoListContext
import javafx.scene.Scene
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import model.IObserver
import model.Presenter
import model.TodoItem
import java.time.LocalDateTime
import java.time.temporal.ChronoField

class Today(private val presenter: Presenter) : Scene(
        BorderPane(),
        presenter.width,
        presenter.height
),
    IObserver {

    val current = LocalDateTime.now()
    var month = current.get(ChronoField.MONTH_OF_YEAR)
    var day = current.get(ChronoField.DAY_OF_MONTH)
    var year = (current.get(ChronoField.YEAR)).toString().takeLast(4)
    var date = "$year-$month-$day"
    var itemList: ItemList = ItemList(date, "end date", false, presenter)

    var welcome = Heading("To-do Items Due Today", itemList, presenter)

    var clipboardHasItem: Boolean = false
    var cutItem: Boolean = false
    var currentUserId: Int = 23
    var curListId: Int = 0

    private fun createLayout() {
        println("today is: ")
        println(date)
        // setup individual elements
        val tool = ToolBarUI(presenter)

        val top = VBox(tool, welcome)


        // add everything to the root
        val root = this.root as BorderPane
        println("today")
        println(presenter.darkmode)
        if (presenter.darkmode) {
            root.style = "-fx-background-color: #1A1A33"
        } else {
            root.style = "-fx-background-color: #F0ECF9"
        }
        root.center = top

        var listContext: TodoListContext = TodoListContext()
        var response = listContext.getList()

        if (response != null) {
            curListId = response.get(0).list_id.toInt()
        }
    }

    // updates are driven by the presenter, not the model
    override fun update() {

    }

    init {
        createLayout()
        update()
    }


    fun copyItem() {
        clipboardHasItem = itemList.currentlyClickedItem != null
        itemList.updateLastActionedItemBox()
        cutItem = false

    }

    fun pasteItem() {
        // no item to paste!
        if (!clipboardHasItem) {
            return
        }

        // there is an item to paste

        try {

            val createCopy = itemList.createCopyOfLastActionedItemBox()

            // add copy to drag and drop list -> done in pasteItem(...)
//            itemList.todoitems.add(createCopy)

            val apiRequest = APIRequests() // helper api request interface
            val listContext = TodoListContext()

            val todoItem = itemList.lastActionedItemBox!!.toTodoItem()
            pasteItem(todoItem, itemList.createCopyOfLastActionedItemBox())

            itemList.flushFutureActions()
        } catch (e: Exception) {
            // if there's any error we should delete the item we posted to the db
        }
    }

    fun deleteCurrentlySelectedItem() {
//        val response = TodoListContext().removeItem(itemList.currentlyClickedItemBox!!.itemID)
        deleteItem(itemList.lastActionedItemBox!!, true, addToUndoStack = false)
        itemList.removeLastActionedItemBox()
//        itemList.updateLastActionedItemBox()
    }

    fun deleteItem(itemBox: ItemBox, removeFromView: Boolean = true, addToUndoStack: Boolean = true, oldIndex: Int = -1) {
        val response = TodoListContext().removeItem(itemBox.itemID)
        if (removeFromView) {
            itemList.removeItemBox(itemBox)
        }
        if (addToUndoStack) {
            itemList.pastActions.addLast(
                UndoRedoInfo(
                ActionType.DELETE,
                listOf(itemList.itemBoxToTodoItem(itemBox)),
                ExtraInfo(oldIndex = oldIndex, itemBox = itemBox)
                )
            )
        }
    }
    fun refreshView() {
        itemList.children.clear()
        itemList.children.addAll(ItemList("", "all", false, presenter).children)
    }

    // itemBoxToAttach must be a NEW itembox :)
    fun pasteItem(todoItem: TodoItem, itemBoxToAttach: ItemBox, doNotAddToUndoRedo: Boolean = false, index: Int = -1) {
        //val createCopy = itemList.createCopyOfLastActionedItemBox()

        val apiRequest = APIRequests() // helper api request interface
        val listContext = TodoListContext()

        todoItem.start_date += " 00:00:00"
        todoItem.end_date += " 00:00:00"

        var r = apiRequest.postItem(todoItem)

        // create list-item link
        val newlyCopiedItem = Gson().fromJson(r, TodoItem::class.java)

        // add this to our internal list that verinoka uses :)
        itemList.todoitems.add(itemBoxToAttach)

        itemBoxToAttach.itemID = newlyCopiedItem.item_id.toInt()

        // add copy of item to internal list used by front-end
        if (index >= 0) {
            itemList.children.add(index, itemBoxToAttach)
        } else {
            itemList.children.add(itemBoxToAttach)
        }

        r = apiRequest.putListToItem(curListId, newlyCopiedItem.item_id)

        // create user-list link
        apiRequest.putUserToList(currentUserId, curListId)

        // everything went well :) - delete the old item if we're cutting (CTRL+X)
        if (cutItem) {

            // tracking for undo/redo
            if (!doNotAddToUndoRedo) {
                itemList.pastActions.addLast(
                    UndoRedoInfo(
                    ActionType.CUT_PASTE,
                    listOf(itemList.lastActionedItem!!, newlyCopiedItem),
                    ExtraInfo(itemList.getIndexOf(itemList.lastActionedItemBox!!), itemList.lastActionedItemBox)
                    )
                )
            }
            deleteCurrentlySelectedItem() // remove cut item from view
            clipboardHasItem = false
            cutItem = false
        } else {
            // revert opacity of old item because it's still there
            itemList.resetOpacityOfCurrentlyClickedItemBox()
            if (!doNotAddToUndoRedo) {
                itemList.pastActions.addLast(UndoRedoInfo(ActionType.COPY_PASTE, listOf(newlyCopiedItem)))
            }
        }

    }

    fun cutItem() {
        clipboardHasItem = itemList.currentlyClickedItem != null
        itemList.updateLastActionedItemBox()
        itemList.makeLastActionedItemBoxCut()
        cutItem = true
    }

    fun undoLastAction() {
        if (itemList.pastActions.isEmpty()) {
            return
        }

        when (itemList.pastActions.last().actionType) {
            ActionType.ADD -> println("todo")
            ActionType.DELETE-> undoDelete()
            ActionType.CUT_PASTE -> undoCutPaste()
            ActionType.COPY_PASTE -> undoCopyPaste(itemList.pastActions.last().list.first())
            else -> {
                return
            }
        }
        val action = itemList.pastActions.removeLast()
        itemList.futureActions.addLast(action)
    }

    private fun undoDelete() {
        val item = itemList.pastActions.last().list[0]
        val box = itemList.pastActions.last().extraInfo.itemBox!!
        pasteItem(item, box, doNotAddToUndoRedo = true, itemList.pastActions.last().extraInfo.oldIndex)
    }
    private fun undoCutPaste() {
        // to undo a copy + paste all we need to do is delete the pasted item
        val tlc = TodoListContext()
        val apiRequest = APIRequests()

        // remove item from db
        val pastedItem = itemList.pastActions.last().list[1] // second item in list the newly copied item
        val response = tlc.removeItem(pastedItem.item_id.toInt())

        // remove item from view // this item changed after redo
        itemList.removeTodoItem(pastedItem)

        // return old item to db
        val cutItem = itemList.pastActions.last().list[0]
        var r = apiRequest.postItem(cutItem)

        // create list-item link
        val returnedItem = Gson().fromJson(r, TodoItem::class.java)

        r = apiRequest.putListToItem(curListId, returnedItem.item_id)

        apiRequest.putUserToList(currentUserId, curListId)

        // returning to DB gives item a new item_id!
        val returnBox = itemList.pastActions.last().extraInfo.itemBox
        returnBox!!.itemID = returnedItem.item_id.toInt()

        // need to add old item back into internal lists
        itemList.addItemToInternalLists(
            returnBox.itemID, // returnBox.itemID
            cutItem.item_title,
            cutItem.item_description,
            cutItem.tag,
            cutItem.start_date,
            cutItem.end_date,
            cutItem.is_flagged,
            cutItem.priority,
            cutItem.is_complete
        )

        // add old item to view
        itemList.children.add(itemList.pastActions.last().extraInfo.oldIndex, returnBox)
        clipboardHasItem = false
        itemList.flushInternalVariables()
    }

    private fun undoCopyPaste(todoItem: TodoItem) {
        // to undo a cut + paste all we need to do is:
        //   delete the pasted item
        //   return the pasted item back to where it was on the display

        // delete
        val response = TodoListContext().removeItem(todoItem.item_id.toInt())

        // remove item from view
        itemList.removeTodoItem(todoItem)
        clipboardHasItem = false
        itemList.flushInternalVariables()
    }

    fun redoLastAction() {
        if (itemList.futureActions.isEmpty()) {
            return
        }

        when (itemList.futureActions.last().actionType) {
            ActionType.ADD -> println("todo")
            ActionType.DELETE-> redoDelete(itemList.futureActions.last())
            ActionType.CUT_PASTE -> redoCutPaste(itemList.futureActions.last().list, itemList.futureActions.last().extraInfo.itemBox!!)
            ActionType.COPY_PASTE -> redoCopyPaste(itemList.futureActions.last().list.first())
            else -> {
                return
            }
        }
        val action = itemList.futureActions.removeLast()
        itemList.flushInternalVariables()
    }

    private fun redoDelete(action: UndoRedoInfo) {
        // must repaste item into correct location
        deleteItem(action.extraInfo.itemBox!!, oldIndex = action.extraInfo.oldIndex)
    }
    private fun redoCutPaste(items: List<TodoItem>, remove: ItemBox) {

        // paste item back in
        val pastedItem = items[1]
        val returnItemBox = itemList.createItemBoxFromTodoItem(pastedItem)
        pasteItem(pastedItem, returnItemBox, doNotAddToUndoRedo = true)

        // delete the item we pasted from
        val _cutItem = items[0]
        val response = TodoListContext().removeItem(_cutItem.item_id.toInt())

        itemList.removeItemBox(remove)

        // all I need to do is remove the old item box
        itemList.removeLastActionedItemBox()
        clipboardHasItem = false
        cutItem = false

        itemList.futureActions.last().extraInfo.itemBox = returnItemBox
        itemList.flushInternalVariables()
    }
    private fun redoCopyPaste(todoItem: TodoItem) {
        pasteItem(todoItem, itemList.createItemBoxFromTodoItem(todoItem))
    }

    fun moveItemUp() {
        itemList.moveUp()
    }

    fun moveItemDown() {
        itemList.moveDown()
    }
}

