package com.whatoodo.desktop

import com.google.gson.Gson
import com.whatoodo.desktop.Components.ItemBox
import com.whatoodo.desktop.ENUMS.ActionType
import model.TodoItem
import model.TodoList
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

// info to keep in stacks for undo/redo logic
class UndoRedoInfo(val actionType: ActionType, val  list: List<TodoItem>, val  extraInfo: ExtraInfo = ExtraInfo()) {}

class ExtraInfo(val oldIndex: Int = -1, var itemBox: ItemBox? = null) { }
class TodoListContext {
    /**
     * current_list
     * add_list
     * edit_list
     * remove_list
     */
    var server_address: String = "http://localhost:8000"

    fun getList(): Array<TodoList>? {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson: Gson = Gson()
        var lists: Array<TodoList>? = gson.fromJson(response.body(), Array<TodoList>::class.java)
        // println(lists.toString())

        return lists
    }

    fun getListByUser(userId: Int): Array<TodoList>? {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list/all-lists/${userId}"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson: Gson = Gson()
        var lists: Array<TodoList>? = gson.fromJson(response.body(), Array<TodoList>::class.java)
        // println(lists.toString())
        return lists
    }

    fun getListById(listId: Int): TodoList {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list/$listId"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson = Gson()
        //        println(list.toString())

        return gson.fromJson(response.body(), TodoList::class.java)
    }

//    fun addList(listname: String, category: String, isFavourite: Boolean) {
//        var list: TodoListPayload = TodoListPayload(listname,category, isFavourite)
//        var gson = Gson()
//        var requestBody: String = gson.toJson(list)
//        println(requestBody)
//
//        val client = HttpClient.newBuilder().build()
//        val endpoint = "$server_address/todo-list"
//        val request = HttpRequest.newBuilder()
//            .uri(URI.create(endpoint))
//            .POST(HttpRequest.BodyPublishers.ofString(requestBody))
//            .build()
//        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
//        println(response.body())
//
//
//
//    }



    fun removeList(listID: Int) {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list/$listID"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).DELETE().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())


    }

    fun removeItem(listItem: Int) : HttpResponse<String>? {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-item/$listItem"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).DELETE().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())

        return response
    }

    fun filterBy(type: String, value: String, listId: Long) : Array<TodoItem>? {
        var gson: Gson = Gson()
        var lists: Array<TodoItem>? = arrayOf()
        val client = HttpClient.newBuilder().build()
        var endpoint:String = ""


        //var lists: Array<TodoList>? = gson.fromJson(response.body(), Array<TodoList>::class.java)

        if (type == "tag") {
            // Endpoint:localhost:8000/todo-item/filter-tag?tag=tag&list_id=id
            endpoint = "$server_address/todo-item/filter-tag?tag=${value}&list_id=$listId"


        } else if (type == "priority") {
            // localhost:8000/todo-item/filter-priority?priority=priority&list_id=id
            endpoint = "$server_address/todo-item/filter-priority?priority=${value}&list_id=$listId"


        } else if (type == "flagged") {
            val boolValue = value.toBoolean()
            // localhost:8000/todo-item/filter-flagged?flagged=flagged&list_id=id
            endpoint = "$server_address/todo-item/filter-flagged?flagged=${boolValue}&list_id=$listId"


        } else if (type == "completed") {
            val boolValue = value.toBoolean()
            // localhost:8000/todo-item/filter-completed?completed=completed&list_id=id
            endpoint = "$server_address/todo-item/filter-completed?completed=${boolValue}&list_id=$listId"


        } else if (type == "start date") {
            // value must be in form  "YYYY-MM-DD"
            // localhost:8000/todo-item/filter-start-date?date=date&list_id=id
            endpoint = "$server_address/todo-item/filter-start-date?date=${value}&list_id=$listId"


        } else if (type == "end date") {
            // value must be in form  "YYYY-MM-DD"
            // localhost:8000/todo-item/filter-end-date?date=date&list_id=id
            endpoint = "$server_address/todo-item/filter-end-date?date=${value}&list_id=$listId"


        } else if (type == "search") {
            // http://localhost:8000/todo-item/search?pattern=:pattern&list_id=:listId
            endpoint = "$server_address/todo-item/search?pattern=${value}&list_id=$listId"



        } else if (type == "search all") {
            // http://localhost:8000/todo-item/search-all?pattern=:pattern&user_id=:userId
            // change to user ID
            endpoint = "$server_address/todo-item/search?pattern=${value}&user_id=$listId"

        }

        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        //lists = gson.fromJson(response.body(), Array<TodoList>::class.java)
        lists = gson.fromJson(response.body(), Array<TodoItem>::class.java)
        return lists
    }


    fun sortBy(type: String, sort: String, listId: Long) : Array<TodoItem>? {
        // sort is always ASC/DESC
        var gson: Gson = Gson()
        var lists: Array<TodoItem>? = arrayOf()
        val client = HttpClient.newBuilder().build()
        var endpoint:String = ""


        //var lists: Array<TodoList>? = gson.fromJson(response.body(), Array<TodoList>::class.java)
        if (type == "title") {
            // endpoint: http://localhost:8000/todo-item/sort-title?sort=:sort&list_id=:id
            endpoint = "$server_address/todo-item/sort-title?sort=${sort}&list_id=$listId"

        } else if (type == "tag") {
            // http://localhost:8000/todo-item/sort-tag?sort=:sort&list_id=:id
            endpoint = "$server_address/todo-item/sort-tag?sort=${sort}&list_id=$listId"


        } else if (type == "priority") {
            // http://localhost:8000/todo-item/sort-priority?sort=:sort&list_id=:id
            endpoint = "$server_address/todo-item/sort-priority?sort=${sort}&list_id=$listId"


        } else if (type == "start date") {
            // value must be in form  "YY-MM-DD"
            // http://localhost:8000/todo-item/sort-start-date?sort=:sort&list_id=:id
            endpoint = "$server_address/todo-item/sort-start-date?sort=${sort}&list_id=$listId"


        } else if (type == "end date") {
            // value must be in form  "YY-MM-DD"
            // http://localhost:8000/todo-item/sort-end-date?sort=:sort&list_id=:id
            endpoint = "$server_address/todo-item/sort-end-date?sort=${sort}&list_id=$listId"
        }

        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        //lists = gson.fromJson(response.body(), Array<TodoList>::class.java)
        lists = gson.fromJson(response.body(), Array<TodoItem>::class.java)
        return lists
    }

}