package com.whatoodo.desktop.Pages

import com.whatoodo.desktop.HelperFunctions.FileReaderHelper
import model.IObserver
import model.Presenter
import com.whatoodo.desktop.Context.userContext
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.PasswordField
import javafx.scene.control.TextField
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.scene.text.Font
import payload.RegistrationPayload

class Signup(private val presenter: Presenter) : Scene(
        BorderPane(),
        presenter.width,
        presenter.height

),
    IObserver {

    val frh: FileReaderHelper = FileReaderHelper()
    var lastErrorMessage = Label()
    private fun createLayout() {
        // setup individual elements
        val img = ImageView(Image(frh.appendURL("account.png"), 450.0, 400.0, true, true))
        val secondLabel = Label("Make an account for What2Do?")
        secondLabel.font = Font.font("Helvetica", 16.0)

        val username = Label("Username:")
        val usernameField = TextField()
        usernameField.promptText = "Username"


        val email = Label("Email:")
        val emailField = TextField()
        emailField.promptText = "Email"

        val password = Label("Password:")
        val passwordField = PasswordField()
        passwordField.promptText = "Password"

        val confirmpassword = Label("Confirm Password:")
        val confirmpasswordField = PasswordField()
        confirmpasswordField.promptText = "Confirm Password"

        val firstName = Label("First name:")
        val firstNameField = TextField()
        firstNameField.promptText = "First name"

        val lastName = Label("Last name:")
        val lastnameField = TextField()
        lastnameField.promptText = "Last name"

        val occupation = Label("Occupation:")
        val occupationField = TextField()
        occupationField.promptText = "Occupation"

        val box2 = VBox(img,
            username,
            usernameField,
            email,
            emailField,
            password,
            passwordField,
            confirmpassword,
            confirmpasswordField,
            firstName,
            firstNameField,
            lastName,
            lastnameField,
            occupation,
            occupationField,

        )



        val top = HBox(box2)
        HBox.setMargin(box2, Insets(10.0))

       //top.spacing = 10.0
        top.alignment = Pos.CENTER

        // forward and back buttons
        val nextButton = Button("Create an account and login")
        nextButton.requestFocus()

        nextButton.onMouseClicked = EventHandler {
            run {
                var userContext = userContext()

                var payload: RegistrationPayload = RegistrationPayload(
                    usernameField.text,
                    emailField.text,
                    passwordField.text,
                    firstNameField.text,
                    lastnameField.text,
                    occupationField.text,
                )

                var newItem = userContext.register(payload)
                //println(newItem)

                if (newItem.error_message == "User successfully registered.") {
                    //println(newItem)
                    presenter.advance()

                } else {
                    var error = Label(newItem.error_message)
                    if (!(lastErrorMessage.text.isEmpty())) {
                        box2.children.remove(lastErrorMessage)
                    }
                    //var error = Label("No account found, please make sure email and password is correct or create a new account")
                    error.styleClass.add("error-label")
                    //ErrorBox.children.addAll(error)
                    box2.children.addAll(error)
                    lastErrorMessage = error
                }

            }
        }

        val backButton = Button("Back to login")
        backButton.requestFocus()
        backButton.onMouseClicked = EventHandler {
            run {
                presenter.welcome()

            }
        }




        val bottom = HBox(backButton, nextButton)
        bottom.alignment = Pos.CENTER_RIGHT
        bottom.spacing = 10.0
        bottom.padding = Insets(30.0)

        // add everything to the root
        val root = this.root as BorderPane
        root.center = top
        root.bottom = bottom
        root.style = "-fx-background-color: white"
    }

    // updates are driven by the presenter, not the model
    override fun update() {
    }

    init {
        createLayout()
        update()
    }
}
