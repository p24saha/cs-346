package com.whatoodo.desktop.Pages

import com.whatoodo.desktop.HelperFunctions.FileReaderHelper
import model.IObserver
import model.Presenter
import com.whatoodo.desktop.Context.userContext
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.PasswordField
import javafx.scene.control.TextField
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import javafx.scene.text.Font
import payload.LoginPayload

class Login(private val presenter: Presenter) : Scene(
        BorderPane(),
        presenter.width,
        presenter.height
),
    IObserver {
    val frh: FileReaderHelper = FileReaderHelper()
    var lastErrorMessage = Label()
    private fun createLayout() {
        // setup information in the center of the screen
        val img = ImageView(Image(frh.appendURL("todo.png"), 450.0, 600.0, true, true))
        val label = Label("Welcome to WhatTooDo?")
        label.font = Font.font("Helvetica", 16.0)

        val label2 = Label("Your personalized todo list that tracks your priorities")
        label2.font = Font.font("Helvetica", 12.0)

        val username = Label("Email:")
        val textField = TextField()
        textField.promptText = "Email"
        textField.setMaxWidth(300.0)


        val email = Label("Password:")
        val textField2 = PasswordField()
        textField2.promptText = "Password"
        textField2.setMaxWidth(300.0)

        val login = Button("Login")
        login.font = Font("Helvetica", 11.0)

        val noAccount = Button("Don't have an account? Make an account!")
        noAccount.font = Font("Helvetica", 11.0)

        val center = VBox( img, label, label2, username, textField, email, textField2)
        VBox.setMargin(img, Insets(10.0))
        center.alignment = Pos.CENTER
        center.spacing = 10.0

        // setup buttons along the bottom


        login.onMouseClicked = EventHandler {
            run {
                var userContext = userContext()

                var payload: LoginPayload = LoginPayload(
                    textField.text,
                    textField2.text
                )

                var newItem = userContext.login(payload)



                // "error_code": "LOGIN_SUCCESS"
                if (newItem.error_message == "User logged in successfully") {
                    //println(newItem)
                    presenter.advance()

                } else {
                    var error = Label(newItem.error_message)
                    if (!(lastErrorMessage.text.isEmpty())) {
                        center.children.remove(lastErrorMessage)
                    }

                    //var error = Label("No account found, please make sure email and password is correct or create a new account")
                    error.styleClass.add("error-label")
                    //ErrorBox.children.addAll(error)
                    center.children.addAll(error)
                    lastErrorMessage = error
                }


            }
        }

        login.requestFocus()

        noAccount.onMouseClicked = EventHandler { mouseEvent: MouseEvent? -> presenter.rewind() }

        noAccount.requestFocus()

        val bottom = VBox(login, noAccount)
        bottom.padding = Insets(30.0)
        bottom.spacing = 10.0
        bottom.alignment = Pos.CENTER_RIGHT

        // add everything to the root
        val root = this.root as BorderPane
        root.center = center
        root.bottom = bottom
        root.style = "-fx-background-color: white"
    }

    // updates are driven by the presenter, not the model
    override fun update() {
        // we would fetch data if we had data on this screen
    }

    init {

        createLayout()
    }
}

