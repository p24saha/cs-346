package com.whatoodo.desktop.Pages

import javafx.application.Application
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.control.ToolBar
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.scene.text.Font
import javafx.stage.Stage

class SigninApplication: Application() {
    override fun start(stage: Stage?) {
        val newWindow = Stage()

        val image = ImageView(Image("file:com/whatoodo/desktop/assets/todo.png", 450.0, 600.0, true, true))
        val label = Label("Welcome to What2Do?")
        label.font = Font.font("Helvetica", 16.0)

        val label2 = Label("Your personalized todo list that tracks your priorities")
        label2.font = Font.font("Helvetica", 12.0)

        val button = Button("Login")
        button.font = Font("Helvetica", 11.0)

        val button2 = Button("Don't have an account? Make an account!")
        button2.font = Font("Helvetica", 11.0)

        val addTask = Button("add tasks")
        val viewAllTasks = Button("view all tasks")
        val categories = Button("categories")
        val today = Button("today items")
        val week = Button("week items")
        val flag = Button("flag")
        val toolbar = ToolBar()


        toolbar.getItems().add(addTask)
        toolbar.getItems().add(viewAllTasks)
        toolbar.getItems().add(categories)
        toolbar.getItems().add(today)
        toolbar.getItems().add(week)
        toolbar.getItems().add(flag)



        button.setOnAction(object:EventHandler<ActionEvent?> {
            override fun handle(event: ActionEvent?) {
                val secondLabel = Label("Welcome to What2Do?")
                secondLabel.font = Font.font("Helvetica", 16.0)

                val sidebyside = HBox(toolbar, secondLabel)

                val welcome = Scene(sidebyside, 500.0, 600.0)

                // New window (Stage)
                val newWindow = Stage()
                newWindow.title = "Welcome page"
                newWindow.scene = welcome

                // Set position of second window, related to primary window.
                //newWindow.x = primaryStage.getX() + 200
                //newWindow.y = primaryStage.getY() + 100
                newWindow.show()

            }})



        button2.onAction = EventHandler{
            val secondLabel = Label("Make an account for What2Do?")
            secondLabel.font = Font.font("Helvetica", 16.0)

            val username = Label("Username:")
            val usernameField = TextField()

            val email = Label("Email:")
            val emailField = TextField()

            val password = Label("Password:")
            val passwordField = TextField()

            val confirmpassword = Label("Confirm Password:")
            val confirmpasswordField = TextField()

            val firstName = Label("First name:")
            val firstNameField = TextField()

            val lastName = Label("Last name:")
            val lastnameField = TextField()

            val occupation = Label("Occupation:")
            val occupationField = TextField()

            val create = Button("Create Account")

            val box2 = VBox(
                username,
                usernameField,
                email,
                emailField,
                password,
                passwordField,
                confirmpassword,
                confirmpasswordField,
                firstName,
                firstNameField,
                lastName,
                lastnameField,
                occupation,
                occupationField,
                create
            )

            val sidebyside = HBox(box2)
            HBox.setMargin(box2, Insets(10.0))
            VBox.setMargin(create, Insets(10.0))

            val secondScene = Scene(sidebyside, 450.0, 500.0)


            // New window (Stage)
            newWindow.title = "Make an account"
            newWindow.scene = secondScene
            newWindow.show()
        }

        val username = Label("Email:")
        val textField = TextField()

        val email = Label("Password:")
        val textField2 = TextField()

        val box = VBox(label, label2, username, textField, email, textField2, button, button2)

        VBox.setMargin(label, Insets(20.0))
        VBox.setMargin(label2, Insets(10.0))
        VBox.setMargin(button, Insets(10.0))
        VBox.setMargin(button2, Insets(10.0))
        VBox.setMargin(textField, Insets(10.0))
        VBox.setMargin(textField2, Insets(10.0))
        VBox.setMargin(email, Insets(10.0))
        VBox.setMargin(username, Insets(10.0))

        val sidebyside = HBox(image, box)
        HBox.setMargin(image, Insets(20.0))
        val scene = Scene(sidebyside, 800.0, 450.0)
        stage?.isResizable = true
        stage?.scene = scene
        stage?.show()
        stage?.title = "Sign up page"

    }
}



