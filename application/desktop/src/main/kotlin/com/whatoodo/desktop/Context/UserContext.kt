package com.whatoodo.desktop.Context

import com.google.gson.Gson
import model.Message
import model.User
import payload.LoginPayload
import payload.RegistrationPayload
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class userContext {
    var server_address: String = "http://localhost:8000"
    var cookie = ""
    fun login(payload: LoginPayload): Message {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/user-auth/login"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        //cookie = response.headers().firstValue("set-cookie").get()
        //println(cookie)
        println(response.body())
        return gson.fromJson(response.body(), Message::class.java)
    }


    fun register(payload: RegistrationPayload) : Message {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/user-auth/register"

        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        println(response.body())
        return gson.fromJson(response.body(), Message::class.java)
    }

    fun getUser() : User {
        val client = HttpClient.newBuilder().build()
        val endpoint = "http://localhost:8000/user-auth/user"
        val httpRequest = HttpRequest.newBuilder()
        val request = httpRequest
            .uri(URI.create(endpoint))
            .header("Set-Cookie", cookie)
            .GET().build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson = Gson()
        println("get user called")
        println(response.body())


        return gson.fromJson(response.body(), User::class.java)

    }

    fun logout() : String {
        var gson = Gson()

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/user-auth/logout"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(""))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        println(response.body())
        return "done"

    }


}