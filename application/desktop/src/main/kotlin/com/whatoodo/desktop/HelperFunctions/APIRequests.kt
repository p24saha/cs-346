package com.whatoodo.desktop.HelperFunctions

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import model.TodoItem
import model.TodoItemNoId
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class APIRequests {
    init {

    }

    val ROOT_ADDRESS: String = "http://localhost:8000"
    fun postItem(todoItem: TodoItem): String {
        val SERVER_ADDRESS = "${ROOT_ADDRESS}/todo-item"
        val string = Json.encodeToString(todoItem)

        val client = HttpClient.newBuilder().build();
        val request = HttpRequest.newBuilder()
                .uri(URI.create(SERVER_ADDRESS))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(string))
                .build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return response.body()

        /*
        Sample JSON to test in postman :)
        {
        "listId": 2,
        "itemID": 10,
        "itemTitle": "Counter-strike global fofensive",
        "itemDescription": "",
        "endDate": "2022-11-30",
        "startDate": "2022-11-30",
        "tag": "Hello World",
        "isFlagged": false,
        "priority": "HIGH"
        }
         */
    }

    fun putListToItem(listId: Int, itemId: Long): String {
        val SERVER_ADDRESS = "${ROOT_ADDRESS}/todo-item/${itemId}/todo-list/${listId}"

        val client = HttpClient.newBuilder().build();
        val request = HttpRequest.newBuilder()
                .uri(URI.create(SERVER_ADDRESS))
                .header("Content-Type", "application/json")
                .PUT(HttpRequest.BodyPublishers.ofString("{\n" +
                        "    \"username\": \"priya\",\n" +
                        "    \"email\": \"priya@gmail.com\",\n" +
                        "    \"password\": \"priya\",\n" +
                        "    \"firstname\": \"priya\",\n" +
                        "    \"lastname\": \"saha\",\n" +
                        "    \"occupation\": \"student\"\n" +
                        "}"))
                .build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body()
    }

    fun putUserToList(userId: Int, listId: Int): String {
        val SERVER_ADDRESS = "${ROOT_ADDRESS}/todo-list/${listId}/user/${userId}"

        val client = HttpClient.newBuilder().build();
        val request = HttpRequest.newBuilder()
                .uri(URI.create(SERVER_ADDRESS))
                .header("Content-Type", "application/json")
                .PUT(HttpRequest.BodyPublishers.ofString("{\n" +
                        "    \"username\": \"priya\",\n" +
                        "    \"email\": \"priya@gmail.com\",\n" +
                        "    \"password\": \"priya\",\n" +
                        "    \"firstname\": \"priya\",\n" +
                        "    \"lastname\": \"saha\",\n" +
                        "    \"occupation\": \"student\"\n" +
                        "}"))
                .build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body()
    }

    fun deleteItem(itemId: Int): String {
        val SERVER_ADDRESS = "${ROOT_ADDRESS}/todo-item/${itemId}"

        val client = HttpClient.newBuilder().build();
        val request = HttpRequest.newBuilder()
                .uri(URI.create(SERVER_ADDRESS))
                .header("Content-Type", "application/json")
                .DELETE()
                .build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body()
    }
}