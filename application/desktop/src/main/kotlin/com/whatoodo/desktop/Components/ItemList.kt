package com.whatoodo.desktop.Components

import model.Presenter
import com.whatoodo.desktop.UndoRedoInfo
import com.whatoodo.desktop.TodoListContext
import com.whatoodo.desktop.Context.userContext

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.layout.VBox
import javafx.scene.paint.Color

import kotlin.math.floor
import model.TodoItem


class ItemList(value: String, type: String, sort: Boolean, private val presenter: Presenter): VBox() {


    var itemID: MutableList<Int> = mutableListOf()
    var itemtitle: MutableList<String> = mutableListOf() //names
    var itemDescription: MutableList<String> = mutableListOf() // groups
    var tag: MutableList<String> = mutableListOf() // shared
    var startDate: MutableList<String> = mutableListOf()
    var endDate: MutableList<String> = mutableListOf() // dates
    var isFlagged: MutableList<Boolean> = mutableListOf() // starred
    var priority1: MutableList<String> = mutableListOf() // priority
    var complete: MutableList<Boolean> = mutableListOf() // complete

    var todoitems: MutableList<ItemBox> = mutableListOf() // used for drag and drop

    // tracking of clicked items (for copy/pasting)
    var currentlyClickedItemBox: ItemBox? = null
    var lastActionedItemBox: ItemBox? = null // this holds the item box that was clicked on during the last action (copy/cut) -> not always the same as current clicked box!

    var currentlyClickedItem: TodoItem? = null
    var lastActionedItem: TodoItem? = null

    // === undo/redo
    // two stacks: one that tracks all past actions (for undo); one for all future actions (for redo)
    // whenever you pop something off the undo stack, you must push it to the redo stack
    // when a new action is created (ex add a new item) -> we must FLUSH the redo stack since we're entering a new sequence of actions

    // what we push to the stack should be the OPPOSITE of what occured
    //  add <-> delete
    //  copy <-> NOTHING.
    //  copy-paste <-> delete
    //  cut-paste <-> delete-add

    val pastActions = ArrayDeque<UndoRedoInfo>()
    val futureActions = ArrayDeque<UndoRedoInfo>()

    // === DRAG AND DROP
    var lists: MutableList<TodoItem> = mutableListOf()

    var startY = 0.0
    var containerHeight = 0.0
    var oldIndex = 0

    // === Action Type and Lists of Items
    init {
        if (presenter.darkmode) {
            background = Background(BackgroundFill(Color.web("#1A1A33"), CornerRadii.EMPTY, Insets.EMPTY))
        } else {
            background = Background(BackgroundFill(Color.web("#F0ECF9"), CornerRadii.EMPTY, Insets.EMPTY))
        }

        alignment = Pos.TOP_CENTER
        padding = Insets(10.0, 10.0, 10.0, 10.0)
        spacing = 10.0

        var lists2: Array<TodoItem>? = arrayOf()
        var id = 0
        var t = 0

        var listContext: TodoListContext = TodoListContext()
        var response = listContext.getList()

        if (response != null) {
            id = response.get(0).list_id.toInt()
        }

        if (type == "all") {
            if (response != null) {
                //println("size is")
                //println(userResponse.size)
                //if (UserId != 0) {
                    //lists = userResponse.get(0).todo_items
                    lists = listContext.getListById(id).todo_items
                    t = 1
                //}
                //println("type")
                //println(t)
            }

        } else if (type == "tag" || type == "title" || type == "priority" || type == "flagged" || type == "completed" || type == "start date" || type == "end date" || type == "search") {
            if (sort) {
                lists2 = listContext.sortBy(type, value, id.toLong())!!
                lists = lists2.toMutableList()
                t = 2

            } else {
                lists2 = listContext.filterBy(type, value, id.toLong())!!
                lists = lists2.toMutableList()
                t = 2
            }

        } else {
            println("error no such type")
        }

        // put in the array
        var r = 0

        if (t == 1) {
            if (lists != null) {
                if (lists.size > 0) {
                    for (item in lists) {

                        itemID.add(r, item.item_id.toInt())
                        itemtitle.add(r, item.item_title)
                        itemDescription.add(r, item.item_description)
                        tag.add(r, item.tag)
                        startDate.add(r, item.start_date.substring(0, 10))
                        endDate.add(r, item.end_date.substring(0, 10))
                        isFlagged.add(r, item.is_flagged)
                        priority1.add(r, item.priority)
                        complete.add(r, item.is_complete)
                        r += 1

                    }
                    for (i in 0..(r - 1)) {

                        var newPriority = 0
                        if (priority1[i] == "HIGH") {
                            newPriority = 2
                        } else if (priority1[i] == "MEDIUM") {
                            newPriority = 1
                        } else {
                            newPriority = 0
                        }

                        val currentItem = ItemBox(
                                id,
                                itemID[i],
                                itemtitle[i],
                                itemDescription[i],
                                tag[i],
                                startDate[i],
                                endDate[i],
                                isFlagged[i],
                                newPriority,
                                complete[i],
                                presenter
                        )
                        currentItem.setOnMouseClicked {
                            resetOpacityOfCurrentlyClickedItemBox()
                            currentlyClickedItemBox = currentItem
                            currentlyClickedItemBox?.opacity = 0.6
                            currentlyClickedItem = lists[i]
                        }
                        children.add(currentItem)
                        todoitems.add(currentItem)
                    }
                } else {
                    val noItem = Label("No items in list")
                    children.add(noItem)
                }
            }

        } else if (t == 2) {

            if (lists2 != null) {
                if (lists2.size > 0) {


                    for (item in lists2) {
                        itemID.add(r, item.item_id.toInt())
                        itemtitle.add(r, item.item_title)
                        itemDescription.add(r, item.item_description)
                        tag.add(r, item.tag)
                        startDate.add(r, item.start_date)
                        endDate.add(r, item.end_date)
                        isFlagged.add(r, item.is_flagged)
                        priority1.add(r, item.priority)
                        complete.add(r, item.is_complete)
                        r += 1

                    }



                    for (i in 0..(r - 1)) {

                        var newPriority = 0
                        if (priority1[i] == "HIGH") {
                            newPriority = 2
                        } else if (priority1[i] == "MEDIUM") {
                            newPriority = 1
                        } else {
                            newPriority = 0
                        }

                        val currentItem = ItemBox(
                            id,
                            itemID[i],
                            itemtitle[i],
                            itemDescription[i],
                            tag[i],
                            startDate[i],
                            endDate[i],
                            isFlagged[i],
                            newPriority,
                            complete[i],
                            presenter
                        )
                        currentItem.setOnMouseClicked {
                            resetOpacityOfCurrentlyClickedItemBox()
                            currentlyClickedItemBox = currentItem
                            currentlyClickedItemBox?.opacity = 0.6
                            currentlyClickedItem = lists[i]
                        }
                        children.add(currentItem)
                        todoitems.add(currentItem)


                    }
                } else {
                    val noItem = Label("No items in list")
                    noItem.styleClass.add("val-label")
                    children.add(noItem)
                }
            }
        }  else {
            val noItem = Label("No items in list")
            noItem.styleClass.add("val-label")
            children.add(noItem)
        }

        children.forEach { node ->

            node.setOnMousePressed { event ->
                todoitems.clear()

                // refresh children (remove hidden children)
                children.removeIf{ child ->
                    !child.isVisible
                }
                children.forEach { child ->
                    todoitems.add(child as ItemBox)
                }

                // logic for copy/cut/paste/undo/redo
                resetOpacityOfCurrentlyClickedItemBox()
                currentlyClickedItemBox = node as ItemBox
                currentlyClickedItemBox?.opacity = 0.6
                currentlyClickedItem = lists[children.indexOf(node)]

                startY = event.sceneY - node.translateY
                containerHeight = height
                oldIndex = floor((node.boundsInParent.minY - 10.39) / 46.4).toInt() + 1
                if (oldIndex == -1) {
                    oldIndex = 0
                } else if (oldIndex > todoitems.count() - 1) {
                    oldIndex = todoitems.count() - 1
                }

            }

            node.setOnMouseDragged { event ->
                var offsetY = event.sceneY - startY
                if (offsetY > 0.0 - node.layoutY && offsetY < containerHeight - node.layoutY) { // check that we are dragging within bounds
                    node.translateY = offsetY
                }
            }

            node.setOnMouseReleased { event ->
                // refresh todoitems (this COULD have changed due to a copy/paste)
                todoitems.clear()

                // refresh children (remove hidden children)
                children.removeIf{ child ->
                    !child.isVisible
                }
                children.forEach { child ->
                    todoitems.add(child as ItemBox)
                }

                // get Y position we dragged to
                var dragPos = node.boundsInParent.minY
                // find new index of event in todoitems
                var newIndex = kotlin.math.floor((dragPos + 23.39) / 46.4).toInt()
                if (node.translateY == 0.0) {
                    newIndex = oldIndex
                }
                if (newIndex >= children.count()) {
                    newIndex = children.count() - 1
                }

                // reorder todoitems array
                var temp: ItemBox = todoitems[oldIndex]
                todoitems.removeAt(oldIndex)
                todoitems.add(newIndex, temp)

                // snap into place
                node.translateY = 0.0

                // clear and re-add children in the new order
                var temp2: ItemBox = children[oldIndex] as ItemBox
                children.removeAt(oldIndex)
                children.add(newIndex, temp2)

            }
        }

    }

    fun addChildNode(itemBox: ItemBox) {
        children.add(itemBox)
    }

    fun attachActionsToItemBox(node: ItemBox) {
        node.setOnMousePressed { event ->

            // logic for copy/cut/paste/undo/redo
            resetOpacityOfCurrentlyClickedItemBox()
            currentlyClickedItemBox = node as ItemBox
            currentlyClickedItemBox?.opacity = 0.6
            currentlyClickedItem = lists[children.indexOf(node)]

            startY = event.sceneY - node.translateY
            containerHeight = height
            oldIndex = floor((node.boundsInParent.minY - 10.39) / 46.4).toInt() + 1
            if (oldIndex == -1) {
                oldIndex = 0
            } else if (oldIndex > todoitems.count() - 1) {
                oldIndex = todoitems.count() - 1
            }

        }

        node.setOnMouseDragged { event ->
            var offsetY = event.sceneY - startY
            if (offsetY > 0.0 - node.layoutY && offsetY < containerHeight - node.layoutY) { // check that we are dragging within bounds
                node.translateY = offsetY
            }
        }

        node.setOnMouseReleased { event ->
            // refresh todoitems (this COULD have changed due to a copy/paste)
            todoitems.clear()

            // refresh children (remove hidden children)
            children.removeIf{ child ->
                !child.isVisible
            }
            children.forEach { child ->
                todoitems.add(child as ItemBox)
            }

            // get Y position we dragged to
            var dragPos = node.boundsInParent.minY
            // find new index of event in todoitems
            var newIndex = kotlin.math.floor((dragPos + 23.39) / 46.4).toInt()
            if (node.translateY == 0.0) {
                newIndex = oldIndex
            }
            if (newIndex >= children.count()) {
                newIndex = children.count() - 1
            }

            // reorder todoitems array

            var temp: ItemBox = todoitems[oldIndex]
            todoitems.removeAt(oldIndex)
            todoitems.add(newIndex, temp)

            // snap into place
            node.translateY = 0.0
            // clear and re-add children in the new order
            children.clear()
            todoitems.forEach { child ->
                children.add(child)
            }

        }
    }
    fun priorityToInt(prio: String): Int {
        return when (prio) {
            "HIGH" -> 2
            "MEDIUM" -> 1
             else -> 0
        }
    }

    fun addItemToInternalLists(
            _itemID: Int,
            _title: String,
            _desc: String,
            _tag: String,
            _startDate: String,
            _endDate: String,
            _flagged: Boolean,
            _priority: String,
            _complete: Boolean
    ) {
        itemID.add(_itemID)
        itemtitle.add(_title)
        itemDescription.add(_desc)
        tag.add(_tag)
        startDate.add(_startDate)
        endDate.add(_endDate)
        isFlagged.add(_flagged)
        priority1.add(_priority)
        complete.add(_complete)
    }

    fun addItemToInternalLists(index: Int) {
        addItemToInternalLists(
                itemID.last() + 1,
                itemtitle[index],
                itemDescription[index],
                tag[index],
                startDate[index],
                endDate[index],
                isFlagged[index],
                priority1[index],
                complete[index]
        )
    }
    fun removeTodoItem(_item: TodoItem) {
        itemID.remove(_item.item_id.toInt())
        itemtitle.remove(_item.item_title)
        itemDescription.remove(_item.item_description)
        tag.remove(_item.tag)
        startDate.remove(_item.start_date)
        endDate.remove(_item.end_date)
        isFlagged.remove(_item.is_flagged)
        priority1.remove(_item.priority)
        complete.remove(_item.is_complete)
        children.removeIf { item: Node ->
            (item as ItemBox).itemID == _item.item_id.toInt()
        }
    }

    fun createCopyOfItemBox(itemBox: ItemBox): ItemBox {
        val node = ItemBox(itemBox.id, itemBox.itemID, itemBox.name,
                itemBox.group, itemBox.shared, itemBox.dueDate, itemBox.dueDate, false, itemBox.priority, itemBox.done, presenter)

        node.setOnMousePressed { event ->
            resetOpacityOfCurrentlyClickedItemBox()
            currentlyClickedItemBox = node as ItemBox
            currentlyClickedItemBox?.opacity = 0.6
            currentlyClickedItem = lists[children.indexOf(node)]

            startY = event.sceneY - node.translateY
            containerHeight = height
            oldIndex = floor((node.boundsInParent.minY - 10.39) / 46.4).toInt() + 1
            if (oldIndex == -1) {
                oldIndex = 0
            } else if (oldIndex > children.size - 1) {
                oldIndex = children.size
            }

        }

        node.setOnMouseDragged { event ->
            var offsetY = event.sceneY - startY
            if (offsetY > 0.0 - node.layoutY && offsetY < containerHeight - node.layoutY) { // check that we are dragging within bounds
                node.translateY = offsetY
            }
        }

        node.setOnMouseReleased { event ->
            // refresh todoitems (this COULD have changed due to a copy/paste)
            todoitems.clear()

            // refresh children (remove hidden children)
            children.removeIf{ child ->
                !child.isVisible
            }
            children.forEach { child ->
                todoitems.add(child as ItemBox)
            }

            // get Y position we dragged to
            var dragPos = node.boundsInParent.minY
            // find new index of event in todoitems
            var newIndex = kotlin.math.floor((dragPos + 23.39) / 46.4).toInt()
            if (node.translateY == 0.0) {
                newIndex = oldIndex
            }
            if (newIndex >= children.count()) {
                newIndex = children.count() - 1
            }

            // reorder todoitems array
            var temp: ItemBox = todoitems[oldIndex]
            todoitems.removeAt(oldIndex)
            todoitems.add(newIndex, temp)

            // snap into place
            node.translateY = 0.0
            // clear and re-add children in the new order
            children.clear()
            todoitems.forEach { child ->
                children.add(child)
            }

        }
        return node
    }
    fun createCopyOfLastActionedItemBox(): ItemBox {
        return createCopyOfItemBox(lastActionedItemBox!!)
    }

    fun createItemBoxFromTodoItem(todoItem: TodoItem): ItemBox {
        val node = ItemBox(
                todoItem.item_id.toInt(),
                todoItem.item_id.toInt(),
                todoItem.item_title,
                todoItem.item_description,
                todoItem.tag,
                todoItem.start_date,
                todoItem.end_date,
                todoItem.is_flagged,
                priorityToInt(todoItem.priority),
                todoItem.is_complete,
                presenter)

        node.setOnMousePressed { event ->
            resetOpacityOfCurrentlyClickedItemBox()
            currentlyClickedItemBox = node as ItemBox
            currentlyClickedItemBox?.opacity = 0.6
            currentlyClickedItem = lists[children.indexOf(node)]

            startY = event.sceneY - node.translateY
            containerHeight = height
            oldIndex = floor((node.boundsInParent.minY - 10.39) / 46.4).toInt() + 1
            if (oldIndex == -1) {
                oldIndex = 0
            } else if (oldIndex > todoitems.count() - 1) {
                oldIndex = todoitems.count() - 1
            }

        }

        node.setOnMouseDragged { event ->
            var offsetY = event.sceneY - startY
            if (offsetY > 0.0 - node.layoutY && offsetY < containerHeight - node.layoutY) { // check that we are dragging within bounds
                node.translateY = offsetY
            }
        }

        node.setOnMouseReleased { event ->
            // refresh todoitems (this COULD have changed due to a copy/paste)
            todoitems.clear()

            // refresh children (remove hidden children)
            children.removeIf{ child ->
                !child.isVisible
            }
            children.forEach { child ->
                todoitems.add(child as ItemBox)
            }

            // get Y position we dragged to
            var dragPos = node.boundsInParent.minY
            // find new index of event in todoitems
            var newIndex = kotlin.math.floor((dragPos + 23.39) / 46.4).toInt()
            if (node.translateY == 0.0) {
                newIndex = oldIndex
            }
            if (newIndex >= children.count()) {
                newIndex = children.count() - 1
            }

            // reorder todoitems array
            var temp: ItemBox = todoitems[oldIndex]
            todoitems.removeAt(oldIndex)
            todoitems.add(newIndex, temp)

            // snap into place
            node.translateY = 0.0
            // clear and re-add children in the new order
            children.clear()
            todoitems.forEach { child ->
                children.add(child)
            }

        }
        return node
    }


    fun resetOpacityOfCurrentlyClickedItemBox() {
        if (currentlyClickedItemBox != null) {
            currentlyClickedItemBox?.opacity = 1.0
        }
    }

    fun intToPrioString(prio: Int): String {
        return when (prio) {
            2 -> "HIGH"
            1 -> "MEDIUM"
            else -> "LOW"
        }
    }

    fun itemBoxToTodoItem(itemBox: ItemBox) =
        TodoItem(
            itemBox.itemID.toLong(),
            itemBox.name,
            itemBox.group,
            intToPrioString(itemBox.priority),
            itemBox.starred,
            itemBox.startDate,
            itemBox.dueDate,
            itemBox.shared,
            itemBox.done
        )

    // ctrl+x (cut) - remove item we "cut"
    fun removeLastActionedItemBox() {
        if (lastActionedItemBox != null) {
            // remove old item from view

            itemID.remove(lastActionedItem!!.item_id.toInt())
            itemtitle.remove(lastActionedItem!!.item_title)
            itemDescription.remove(lastActionedItem!!.item_description)
            tag.remove(lastActionedItem!!.tag)
            startDate.remove(lastActionedItem!!.start_date)
            endDate.remove(lastActionedItem!!.end_date)
            isFlagged.remove(lastActionedItem!!.is_flagged)
            priority1.remove(lastActionedItem!!.priority)
            complete.remove(lastActionedItem!!.is_complete)

            children.remove(lastActionedItemBox)

            lastActionedItemBox = null
            lastActionedItem = null
            updateLastActionedItemBox()
        }
    }

    fun removeItemBox(itemBox: ItemBox) {
        children.remove(itemBox)
    }
    fun updateLastActionedItemBox() {
        lastActionedItemBox = currentlyClickedItemBox
        lastActionedItem = currentlyClickedItem
    }

    fun makeLastActionedItemBoxCut() {
        lastActionedItemBox?.opacity = 0.25
    }

    fun flushInternalVariables() {
        resetOpacityOfCurrentlyClickedItemBox()
        currentlyClickedItemBox = null
        lastActionedItemBox = null
        currentlyClickedItem = null
        lastActionedItem = null
    }

    fun addItemBoxFromTodoItem(todoItem: TodoItem) {

        children.add(
                ItemBox(
                        itemID.last() + 1,
                        todoItem.item_id.toInt(),
                        todoItem.item_title,
                        todoItem.item_description,
                        todoItem.tag,
                        todoItem.start_date,
                        todoItem.end_date,
                        todoItem.is_flagged,
                        priorityToInt(todoItem.priority),
                        todoItem.is_complete,
                        presenter
                )
        )
    }

    fun flushFutureActions() { futureActions.clear() }

    fun getIndexOf(itemBox: ItemBox): Int {
        return children.indexOf(itemBox)
    }

    fun moveUp() {
        // we are just swapping the current item with the next item
        var oldIndex = getIndexOf(currentlyClickedItemBox!!)

        if (oldIndex - 1 >= 0) { // check we are within bounds

            children.remove(currentlyClickedItemBox!!)
            children.add(oldIndex - 1, currentlyClickedItemBox)

        }
    }

    fun moveDown() {
        // we are just swapping the current item with the next item
        var oldIndex = getIndexOf(currentlyClickedItemBox!!)

        if (oldIndex + 1 < children.count()) { // check we are within bounds

            children.remove(currentlyClickedItemBox!!)
            children.add(oldIndex + 1, currentlyClickedItemBox)

        }
    }
}