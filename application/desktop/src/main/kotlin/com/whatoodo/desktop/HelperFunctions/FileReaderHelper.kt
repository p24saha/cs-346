package com.whatoodo.desktop.HelperFunctions

class FileReaderHelper {
    // set this to false when we want to package for others :)
    private val buildWithIntelliJ = true

    fun appendURL(str: String, isAbsolute: Boolean = false): String {
        // this function properly creates the root/relative URL to search for our asset str in
        //   when building in IntelliJ, it's easy to find our assets
        //   but when we start to package our project for others, images fail to load :(
        //   this is my best current workaround; using a hard coded flag.
        return if (isAbsolute) {
            absoluteURL() + str
        } else {
            relativeURL() + str
        }
    }

    private fun relativeURL(): String {
        return if (buildWithIntelliJ){
            "com/whatoodo/desktop/assets/"
        } else {
            "file:assets/"
        }
    }

    private fun absoluteURL(): String {
        return if (buildWithIntelliJ) {
            "src/main/resources/com/whatoodo/desktop/assets/"
        } else {
            "file:assets/"
        }
    }
}
