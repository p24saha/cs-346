package com.whatoodo.desktop.HelperFunctions

import com.google.gson.Gson
import com.whatoodo.desktop.HelperFunctions.FileReaderHelper
import model.SettingsModel
import java.io.File

const val fileName: String = "settings.json"

class SettingsHelper {
    private val frh = FileReaderHelper()
    private val path = frh.appendURL(fileName, isAbsolute = true)
    private val file = File(path)
    private val settingsString = file.inputStream().readBytes().toString(Charsets.UTF_8)
    val settings: SettingsModel = Gson().fromJson(settingsString, SettingsModel::class.java)

    fun saveSettings(settingsModel: SettingsModel) {
        // write to json file
//        Gson().toJson(settingsModel, FileWriter(path))
        file.writeText(settingsModel.toJSONString())
    }

}