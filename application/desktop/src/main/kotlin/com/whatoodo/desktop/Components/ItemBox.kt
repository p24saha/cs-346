package com.whatoodo.desktop.Components


import com.whatoodo.desktop.HelperFunctions.FileReaderHelper
import model.Presenter
import com.whatoodo.desktop.Context.TodoItemContext
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import javafx.scene.paint.Color
import model.TodoItem
import payload.TodoItemPayload

// var priority: String

class ItemBox(// persistent fields to allow deep copies
        val id: Int, var itemID: Int, val name: String, val group: String, val shared: String, val startDate: String, val dueDate: String, var starred: Boolean, var priority: Int, var done: Boolean, private val presenter: Presenter
): HBox() {

    val frh: FileReaderHelper = FileReaderHelper()


    init {


        // checkbox
        var checkBox = Button()
        checkBox.styleClass.clear()
        checkBox.styleClass.add("button-transparent")


        var checkedBox = ImageView(Image(frh.appendURL("checkedbox.png")))
        val unCheckedBox = ImageView(Image(frh.appendURL("uncheckedbox.png")))

        checkedBox.fitHeight = 20.0
        checkedBox.isPreserveRatio = true

        unCheckedBox.fitHeight = 20.0
        unCheckedBox.isPreserveRatio = true

        if (done) {
            checkBox.graphic = checkedBox
        } else {
            checkBox.graphic = unCheckedBox
        }

        checkBox.setOnMouseClicked {
            run {
                // change checkbox graphic
                if (!done) {
                    checkBox.graphic = checkedBox
                } else {
                    checkBox.graphic = unCheckedBox
                }

                var todoitemcontext = TodoItemContext()

                // add PUT request to update done status
                var payload = TodoItemPayload(
                    name,
                    group,
                    intToPriority(priority),
                    starred,
                    "$startDate 05:00:00",
                    "$dueDate 05:00:00",
                    shared,
                    !(done)
                )

                todoitemcontext.editItem(itemID.toLong(), payload)
                done = !done

                presenter.advance()
            }
        }

        // initialize icons

        // GROUP SECTION

        var groupIcon = ImageView(Image(frh.appendURL("group.png")))
        groupIcon.fitHeight = 20.0
        groupIcon.isPreserveRatio = true

        val groupBox = HBox(Label(group), groupIcon)
        groupBox.alignment = Pos.CENTER
        groupBox.spacing = 4.0

        // TAG SECTION

        var tagIcon = ImageView(Image(frh.appendURL("tag.png")))
        tagIcon.fitHeight = 20.0
        tagIcon.isPreserveRatio = true

        val tagBox = HBox(Label(shared), tagIcon)
        tagBox.alignment = Pos.CENTER
        tagBox.spacing = 4.0

        // CALENDAR SECTION

        var calendarIcon = ImageView(Image(frh.appendURL("calendar.png")))
        calendarIcon.fitHeight = 20.0
        calendarIcon.isPreserveRatio = true

        val date = dueDate.take(10)


        val dateBox = HBox(Label(date), calendarIcon)
        dateBox.alignment = Pos.CENTER
        dateBox.spacing = 4.0

        // STARRED

        var filledStar = ImageView(Image(frh.appendURL("star_filled.png")))
        val unfilled_star = ImageView(Image(frh.appendURL("star_unfilled.png")))



        filledStar.fitHeight = 20.0
        filledStar.isPreserveRatio = true

        unfilled_star.fitHeight = 20.0
        unfilled_star.isPreserveRatio = true

        var starBtn = Button()
        if (starred) {
            // fill in star
            starBtn.graphic = filledStar


        } else {
            starBtn.graphic = unfilled_star

        }

        starBtn.styleClass.clear()
        starBtn.styleClass.add("button-transparent")

        starBtn.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {
                if (starred) {
                    starBtn.graphic = unfilled_star


                } else {
                    starBtn.graphic = filledStar


                }

                var todoitemcontext = TodoItemContext()

                // add PUT request to update starred status
                var payload = TodoItemPayload(
                    name,
                    group,
                    intToPriority(priority),
                    !(starred),
                    "$startDate 05:00:00",
                    "$dueDate 05:00:00",
                    shared,
                    done
                )

                todoitemcontext.editItem(itemID.toLong(), payload)
                starred = !starred
                presenter.advance()

            }
        }

        // priority
        var low = ImageView(Image(frh.appendURL("low.png")))
        low.fitHeight = 20.0
        low.isPreserveRatio = true
        val medium = ImageView(Image(frh.appendURL("medium.png")))
        medium.fitHeight = 20.0
        medium.isPreserveRatio = true
        var high = ImageView(Image(frh.appendURL("high.png")))
        high.fitHeight = 20.0
        high.isPreserveRatio = true

        var proprityIcon = Button()
        var priorityText = ""
        proprityIcon.styleClass.add("button-transparent")
        if (priority == 0) {
            proprityIcon.graphic = low
            priorityText = "Low"
        } else if (priority == 1) {
            proprityIcon.graphic = medium
            priorityText = "Medium"
        } else {
            proprityIcon.graphic = high
            priorityText = "High"
        }

        val priorityBox = HBox(Label(priorityText), proprityIcon)
        priorityBox.alignment = Pos.CENTER
        priorityBox.spacing = 4.0



        // edit
        var editIcon = Button()
        editIcon.styleClass.add("button-transparent")

        var icon = ImageView(Image(frh.appendURL("edit.png")))
        icon.fitHeight = 20.0
        icon.isPreserveRatio = true
        editIcon.graphic = icon


        editIcon.onMouseClicked = EventHandler { mouseEvent: MouseEvent? ->
            run {
                presenter.advance(itemID.toLong(), name, group, shared, startDate, dueDate, starred, priority, done)
            }
        }



        // delete

        var deleteIcon = Button()
        deleteIcon.styleClass.add("button-transparent")

        var delete =  ImageView(Image(frh.appendURL("delete.png")))
        delete.fitHeight = 20.0
        delete.isPreserveRatio = true
        deleteIcon.graphic = delete

        deleteIcon.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {
                presenter.deleteItemBox(this)
            }
        }

        val spacer = Pane()

        prefWidth  = 99999.9
        children.addAll(checkBox, Label(name), spacer, groupBox, tagBox, dateBox, starBtn, priorityBox, editIcon, deleteIcon)
        spacing = 10.0
        setHgrow(spacer, Priority.ALWAYS)
        background = Background(BackgroundFill(Color.WHITE, CornerRadii(12.0), Insets.EMPTY))
        alignment = Pos.CENTER
        padding = Insets(5.0, 10.0, 5.0, 10.0)
    }

    fun intToPriority(prio: Int): String {
        return when (prio) {
            2 -> "HIGH"
            1 -> "MEDIUM"
            else -> "LOW"
        }
    }
    fun toTodoItem(): TodoItem {
        return TodoItem(id.toLong(), name, group, intToPriority(priority), true, dueDate, dueDate, shared, done)
    }

}