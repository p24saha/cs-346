package com.whatoodo.desktop.Components

import com.whatoodo.desktop.HelperFunctions.FileReaderHelper
import model.Presenter
import com.whatoodo.desktop.Context.userContext
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.ToolBar
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.*

class ToolBarUI(private val presenter: Presenter) :
    VBox() {

    val frh: FileReaderHelper = FileReaderHelper()
    private fun createLayout() {
        // setup individual elements
        var img = ImageView(Image(frh.appendURL("Union.png"), 100.0, 100.0, true, true))
        var dark = ImageView(Image(frh.appendURL("darkMode.png"), 100.0, 100.0, true, true))
        var light = ImageView(Image(frh.appendURL("lightMode.png"), 100.0, 100.0, true, true))

        if (presenter.darkmode) {
            img = ImageView(Image(frh.appendURL("DarkMode_logo.png"), 100.0, 100.0, true, true))
            dark = ImageView(Image(frh.appendURL("DarkMode_dark.png"), 100.0, 100.0, true, true))
            light = ImageView(Image(frh.appendURL("DarkMode_light.png"), 100.0, 100.0, true, true))

        } else {
            img = ImageView(Image(frh.appendURL("Union.png"), 100.0, 100.0, true, true))
            dark = ImageView(Image(frh.appendURL("darkMode.png"), 100.0, 100.0, true, true))
            light = ImageView(Image(frh.appendURL("lightMode.png"), 100.0, 100.0, true, true))

        }


      
        dark.fitHeight = 20.0
        dark.isPreserveRatio = true

        light.fitHeight = 20.0
        light.isPreserveRatio = true

        var mode = Button()
        mode.styleClass.add("button-transparent")
        if (presenter.darkmode) {
            mode.graphic = dark
        } else {
            mode.graphic = light
        }


        
        val addTask = Button("add tasks")
        val viewAllTasks = Button("view all tasks")
        val today = Button("today's items ")
        //val hotkeysInst = Button("hotkeys instructions")
        val toolbar = ToolBar()
        toolbar.getItems().add(img)
        toolbar.getItems().add(addTask)
        //toolbar.getItems().add(hotkeysInst)
        //hotkeysInst.onMouseClicked = EventHandler { presenter.inst() }
        addTask.onMouseClicked = EventHandler { presenter.add() }
        toolbar.getItems().add(viewAllTasks)
        viewAllTasks.onMouseClicked = EventHandler { presenter.view() }
        toolbar.getItems().add(today)
        today.onMouseClicked = EventHandler { presenter.today() }
        val logout = Button("logout")
        logout.requestFocus()
        toolbar.getItems().add(logout)
        var filterSpacer = Pane()
        HBox.setHgrow(filterSpacer, Priority.ALWAYS)
        toolbar.getItems().add(filterSpacer)

        logout.onMouseClicked = EventHandler {
            run {
                var userContext = userContext()
                var newItem = userContext.logout()
                println(newItem)

                presenter.welcome()
            }
        }


//        toolbar.getItems().add(undo)
//        toolbar.getItems().add(redo)
//        toolbar.getItems().add(copy)
//        toolbar.getItems().add(cut)
//        toolbar.getItems().add(paste)
          toolbar.getItems().add(mode)



        mode.onMouseClicked = EventHandler {
                run {
                    if (presenter.darkmode) {
                        mode.graphic = light
                        presenter.darkmode = false

                    } else {
                        mode.graphic = dark
                        presenter.darkmode = true

                    }
                    presenter.refresh()
                }
        }

        children.add(toolbar)

    }

    // updates are driven by the presenter, not the model


    init {
        createLayout()
    }
}
