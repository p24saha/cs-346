package com.whatoodo.desktop.Components

import com.whatoodo.desktop.HelperFunctions.FileReaderHelper
import model.Presenter
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*


class Heading(name: String, childrenList: ItemList, private val presenter: Presenter): VBox() {


    val frh: FileReaderHelper = FileReaderHelper()
    init {

        var container = HBox()

        var title = Label(name)
        title.styleClass.clear()
        title.styleClass.add("heading-label")

        var searchIcon = ImageView(Image(frh.appendURL("search.png")))
        var sortIcon = ImageView(Image(frh.appendURL("sort.png")))
        var filterIcon = ImageView(Image(frh.appendURL("filter.png")))
        var menuIcon = ImageView(Image(frh.appendURL("menu.png")))

        if (presenter.settings.darkmode) {
            searchIcon = ImageView(Image(frh.appendURL("DarkMode_search.png")))
            sortIcon = ImageView(Image(frh.appendURL("DarkMode_Sort.png")))
            filterIcon = ImageView(Image(frh.appendURL("DarkMode_filter.png")))
            menuIcon = ImageView(Image(frh.appendURL("DarkMode_menu.png")))

        } else {
            searchIcon = ImageView(Image(frh.appendURL("search.png")))
            sortIcon = ImageView(Image(frh.appendURL("sort.png")))
            filterIcon = ImageView(Image(frh.appendURL("filter.png")))
            menuIcon = ImageView(Image(frh.appendURL("menu.png")))

        }


        // spacer
        var spacer = Pane()
        HBox.setHgrow(spacer, Priority.ALWAYS)

        // search and filtering icons

        searchIcon.fitHeight = 25.0
        searchIcon.isPreserveRatio = true

        menuIcon.fitHeight = 25.0
        menuIcon.isPreserveRatio = true


        filterIcon.fitHeight = 25.0
        filterIcon.isPreserveRatio = true

        sortIcon.fitHeight = 22.0
        sortIcon.isPreserveRatio = true

        var searchBtn = Button()
        searchBtn.graphic = searchIcon
        searchBtn.styleClass.clear()
        searchBtn.styleClass.add("button-transparent")

        // FILTER BUTTON

        var filterBtn = Button()
        filterBtn.graphic = filterIcon
        filterBtn.styleClass.clear()
        filterBtn.styleClass.add("button-transparent")

        var sortBtn = Button()
        sortBtn.graphic = sortIcon
        sortBtn.styleClass.clear()
        sortBtn.styleClass.add("button-transparent")

        var menu = Button()
        menu.graphic = menuIcon
        menu.styleClass.clear()
        menu.styleClass.add("button-transparent")


        container.padding = Insets(10.0, 20.0, 10.0, 20.0)
        container.alignment = Pos.CENTER_LEFT
        container.spacing = 15.0
        container.children.addAll(title, spacer, menu, searchBtn, filterBtn, sortBtn)

        children.add(container)

        // MENU CONTAINER
        var menuContainer = HBox()
        val field = Label("undo, redo, copy, cut, paste")
        field.styleClass.add("val-label")

        var spacerMenu = Pane()
        HBox.setHgrow(spacerMenu, Priority.ALWAYS)

        var undo_l = ImageView(Image(frh.appendURL("undo.png"), 100.0, 100.0, true, true))
        var redo_l = ImageView(Image(frh.appendURL("redo.png"), 100.0, 100.0, true, true))
        var copy_l = ImageView(Image(frh.appendURL("copy.png"), 100.0, 100.0, true, true))
        var cut_l = ImageView(Image(frh.appendURL("cut.png"), 100.0, 100.0, true, true))
        var paste_l = ImageView(Image(frh.appendURL("paste.png"), 100.0, 100.0, true, true))

        if (presenter.darkmode) {
            undo_l = ImageView(Image(frh.appendURL("DarkMode_undo.png"), 100.0, 100.0, true, true))
            redo_l = ImageView(Image(frh.appendURL("DarkMode_redo.png"), 100.0, 100.0, true, true))
            copy_l = ImageView(Image(frh.appendURL("DarkMode_copy.png"), 100.0, 100.0, true, true))
            cut_l = ImageView(Image(frh.appendURL("DarkMode_cut.png"), 100.0, 100.0, true, true))
            paste_l = ImageView(Image(frh.appendURL("DarkMode_paste.png"), 100.0, 100.0, true, true))


        } else {
            undo_l = ImageView(Image(frh.appendURL("undo.png"), 100.0, 100.0, true, true))
            redo_l = ImageView(Image(frh.appendURL("redo.png"), 100.0, 100.0, true, true))
            copy_l = ImageView(Image(frh.appendURL("copy.png"), 100.0, 100.0, true, true))
            cut_l = ImageView(Image(frh.appendURL("cut.png"), 100.0, 100.0, true, true))
            paste_l = ImageView(Image(frh.appendURL("paste.png"), 100.0, 100.0, true, true))
        }


        undo_l.fitHeight = 20.0
        undo_l.isPreserveRatio = true

        redo_l.fitHeight = 20.0
        redo_l.isPreserveRatio = true

        copy_l.fitHeight = 20.0
        copy_l.isPreserveRatio = true

        cut_l.fitHeight = 20.0
        cut_l.isPreserveRatio = true

        paste_l.fitHeight = 20.0
        paste_l.isPreserveRatio = true

        var undo = Button()
        undo.styleClass.add("button-transparent")
        undo.graphic = undo_l
        undo.setOnMouseClicked {
            event -> presenter.undoAction()

        }

        var redo = Button()
        redo.styleClass.add("button-transparent")
        redo.graphic = redo_l
        redo.setOnMouseClicked {
            presenter.redoAction()
        }

        var copy = Button()
        copy.styleClass.add("button-transparent")
        copy.graphic = copy_l
        copy.setOnMouseClicked {
            presenter.copyAction()
        }

        var cut = Button()
        cut.styleClass.add("button-transparent")
        cut.graphic = cut_l
        cut.setOnMouseClicked {
            presenter.cutAction()
        }

        var paste = Button()
        paste.styleClass.add("button-transparent")
        paste.graphic = paste_l
        paste.setOnMouseClicked {
            presenter.pasteAction()
        }


        menuContainer.padding = Insets(0.0, 10.0, 0.0, 10.0)
        menuContainer.alignment = Pos.CENTER_LEFT
        menuContainer.spacing = 15.0

        menuContainer.children.addAll(field, spacerMenu, undo, redo, copy, cut, paste)
        menuContainer.isVisible = false
        menuContainer.isManaged = false

        children.add(menuContainer)



        // SEARCH CONTAINER

        var searchContainer = HBox()
        val searchField = TextField()
        searchField.promptText = "Search by name, and tags, ..."
        HBox.setHgrow(searchField, Priority.ALWAYS)
        var submitSearch = Button("Search")



        searchContainer.padding = Insets(0.0, 10.0, 0.0, 10.0)
        searchContainer.alignment = Pos.CENTER_LEFT
        searchContainer.spacing = 15.0

        searchContainer.children.addAll(searchField, submitSearch)
        searchContainer.isVisible = false
        searchContainer.isManaged = false

        children.add(searchContainer)

        // FILTERING CONTAINER

        var filterContainer = HBox()
        var filterLabel = Label("Filter by:")
        filterLabel.styleClass.add("val-label")
        var filterOptions = ComboBox<String>()
        // all the different catagories
        filterOptions.items.addAll("tag", "priority", "completed", "flagged", "start date", "end date")
        var filter = filterOptions.getValue()


        var filterValue = ""

        var select = Button("Select")


        var filterSpacer = Pane()
        HBox.setHgrow(filterSpacer, Priority.ALWAYS)
        var submitFilter = Button("Filter")

        // priority
        val priLabel = Label("Filter by Priority:")
        priLabel.styleClass.add("val-label")
        var priGroup = ToggleGroup()
        var low = RadioButton("Low")
        low.styleClass.add("val-label")
        var med = RadioButton("Medium")
        med.styleClass.add("val-label")
        var hi = RadioButton("High")
        hi.styleClass.add("val-label")
        low.toggleGroup = priGroup
        med.toggleGroup = priGroup
        hi.toggleGroup = priGroup


        // true/false
        val tLabel = Label("Filter by:")
        tLabel.styleClass.add("val-label")
        var fGroup = ToggleGroup()
        var tru = RadioButton("Yes")
        tru.styleClass.add("val-label")
        var fal = RadioButton("No")
        fal.styleClass.add("val-label")
        tru.toggleGroup = fGroup
        fal.toggleGroup = fGroup

        var filterLabel3 = Label("Value:")
        filterLabel3.styleClass.add("val-label")
        var filterText3 = TextField()

        var filterLabel4 = Label("Value in \"YYYY-MM-DD\":")
        filterLabel4.styleClass.add("val-label")
        var filterText4 = TextField()


        select.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {
                filter = filterOptions.getValue()

                if (filter == "priority" ) {

                    filterContainer.children.addAll(priLabel, low, med, hi, filterSpacer, submitFilter)
                }
                else if (filter == "completed" || filter == "flagged") {


                    filterContainer.children.addAll(tLabel, tru, fal, filterSpacer, submitFilter)

                } else if (filter == "tag") {

                    filterContainer.children.addAll(filterLabel3, filterText3, filterSpacer, submitFilter)

                }  else if (filter == "start date" || filter == "end date") {

                    filterContainer.children.addAll(filterLabel4, filterText4, filterSpacer, submitFilter)
                }


            }
        }


        filterContainer.padding = Insets(0.0, 10.0, 0.0, 10.0)
        filterContainer.alignment = Pos.CENTER_LEFT
        filterContainer.spacing = 15.0

        filterContainer.children.addAll(filterLabel, filterOptions, select)
        filterContainer.isVisible = false
        filterContainer.isManaged = false

        children.add(filterContainer)

        // SORTING CONTAINER




        val sortBoxs = HBox()
        val sortBy = Label("Sort by:")
        sortBy.styleClass.add("val-label")
        var sortOptions = ComboBox<String>()
        sortOptions.items.addAll("tag", "title", "priority", "start date", "end date")



        var sortGroup = ToggleGroup()
        var ascending = RadioButton("ascending")
        ascending.styleClass.add("val-label")
        var descending = RadioButton("descending")
        descending.styleClass.add("val-label")

        ascending.toggleGroup = sortGroup
        descending .toggleGroup = sortGroup

        var sortSpacer = Pane()
        HBox.setHgrow(sortSpacer, Priority.ALWAYS)
        var submitSort = Button("Sort")


        sortBoxs.padding = Insets(0.0, 10.0, 0.0, 10.0)
        sortBoxs.alignment = Pos.CENTER_LEFT
        sortBoxs.spacing = 15.0
        sortBoxs.children.addAll(sortBy, sortOptions, ascending, descending, sortSpacer, submitSort )

        sortBoxs.isVisible = false
        sortBoxs.isManaged = false


        children.add(sortBoxs)

        var listItem = HBox()
        //listItem.setPrefWidth(1000.0)
        listItem.alignment = Pos.CENTER
        listItem.children.addAll(childrenList)
        children.add(listItem)

        // BUTTONS
        submitSearch.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {
                var text = searchField.text
                listItem.children.clear()
                listItem.children.add(ItemList(text, "search", false, presenter))
            }
        }

        submitFilter.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {
                filter = filterOptions.getValue()

                if (filter == "priority") {
                    filterValue = if (med.isSelected) {
                        "MEDIUM"
                    } else if (low.isSelected) {
                        "LOW"
                    } else {
                        "HIGH"
                    }
                } else if (filter == "completed" || filter == "flagged") {
                    filterValue = if (tru.isSelected) {
                        "true"
                    } else {
                        "false"
                    }
                } else if (filter == "tag") {
                    filterValue = filterText3.text
                } else if (filter == "start date" || filter == "end date") {
                    filterValue = filterText4.text
                }

                listItem.children.clear()

                listItem.children.add(ItemList(filterValue, filter, false, presenter))
            }
        }

        submitSort.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {
                var value = ""
                value = if (ascending.isSelected) {
                    "ASC"
                } else if (descending.isSelected) {
                    "DESC"
                } else {
                    ""
                }

                var sort = sortOptions.getValue()


                // clear all
                // children.add(ItemList(priority, "priority"))
                listItem.children.clear()
                listItem.children.add(ItemList(value, sort, true, presenter))

            }
        }



        // SEARCH BUTTON RESPONSE

        searchBtn.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {

                if (searchContainer.isVisible) {
                    searchContainer.isVisible = false
                    searchContainer.isManaged = false

                } else {
                    searchContainer.isVisible = true
                    searchContainer.isManaged = true
                }
            }
        }

        // FILTER BUTTON RESPONSE

        filterBtn.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {

                if (filterContainer.isVisible) {
                    filterContainer.isVisible = false
                    filterContainer.isManaged = false

                } else {
                    filterContainer.isVisible = true
                    filterContainer.isManaged = true
                }
            }
        }

        sortBtn.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {

                if (sortBoxs.isVisible) {
                    sortBoxs.isVisible = false
                    sortBoxs.isManaged = false

                } else {
                    sortBoxs.isVisible = true
                    sortBoxs.isManaged = true
                }
            }
        }

        menu.onMouseClicked = EventHandler{ mouseEvent: MouseEvent? ->
            run {

                if (menuContainer.isVisible) {
                    menuContainer.isVisible = false
                    menuContainer.isManaged = false

                } else {
                    menuContainer.isVisible = true
                    menuContainer.isManaged = true
                }
            }
        }

        spacing = 10.0


    }
}
