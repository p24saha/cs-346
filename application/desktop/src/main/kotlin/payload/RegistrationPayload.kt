package payload



data class RegistrationPayload(
    var username: String,
    var email: String,
    var password: String,
    var firstname: String,
    var lastname: String,
    var occupation: String,
)