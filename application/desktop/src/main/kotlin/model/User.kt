package model

import com.google.gson.annotations.Expose
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import model.TodoList

@Serializable
data class User (
    var userid: Int,
    val username: String,
    val email: String,
    val password: String,
    val firstname: String?,
    val lastname: String?,
    val occupation: String?,
    var lists: MutableList<Long> = mutableListOf()
    //@Expose var todoList: MutableList<TodoItem> = mutableListOf<TodoItem>()

) {
    override fun toString(): String {
        return "TodoItem: $userid\n" +
                "   name: $username\n"
    }
}





