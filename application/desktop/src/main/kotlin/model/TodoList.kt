package model

import com.google.gson.annotations.Expose

class TodoList {
    @Expose var list_id: Long = 0
    var list_name: String = ""
    var category: String = ""
    var is_favourite: Boolean = false
    @Expose var todo_items: MutableList<TodoItem> = mutableListOf<TodoItem>()

    constructor(listname: String, category: String, isFavourite: Boolean): this() {
        this.list_name = listname
        this.category = category
        this.is_favourite = isFavourite
    }

    constructor()

    override fun toString(): String {
        var response = "$list_name: $list_id\n" +
                "   Category: $category, Favourite: $is_favourite\n"
        for(item in todo_items) {
            response = response + item.toString() + "\n"
        }

        return response

    }
}
