package model

import kotlinx.serialization.Serializable

@Serializable
data class SettingsModel(
        var width: Double,
        var height: Double,
        var x: Double, // top left
        var y: Double, // top left
        var darkmode: Boolean
) {
    // TODO - use a Gson().toJSON() to serialize this dynamically
    fun toJSONString(): String {
        return "{\n" +
                "  \"width\": $width,\n" +
                "  \"height\": $height,\n" +
                "  \"x\":  $x,\n" +
                "  \"y\": $y,\n" +
                "  \"darkmode\": $darkmode\n" +
                "}\n" +
                "\n"
    }
}