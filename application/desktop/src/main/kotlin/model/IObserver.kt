package model

interface IObserver {
    fun update()
}
