package model

import kotlinx.serialization.Serializable

@Serializable
data class TodoItem (
    var item_id: Long,
    var item_title: String,
    var item_description: String,
    var priority: String,
    var is_flagged: Boolean,
    var start_date: String,
    var end_date: String,
    var tag: String,
    var is_complete: Boolean
) {
    override fun toString(): String {
        return "TodoItem: $item_id\n" +
                "   Title: $item_title\n" +
                "   Description: $item_description\n" +
                "   Priority: $priority\n" +
                "   Flagged: $is_flagged\n" +
                "   Start Date: $start_date\n" +
                "   End Date: $end_date\n" +
                "   Tag: $tag\n" +
                "   Completed: $is_complete\n"

    }
}

@Serializable
data class TodoItemNoId (
        var item_title: String,
        var item_description: String,
        var priority: Int,
        var is_flagged: Boolean,
        var start_date: String,
        var end_date: String,
        var tag: String,
        var is_complete: Boolean
) {
    override fun toString(): String {
        return "TodoItem " +
                "   Title: $item_title\n" +
                "   Description: $item_description\n" +
                "   Priority: $priority\n" +
                "   Flagged: $is_flagged\n" +
                "   Start Date: $start_date\n" +
                "   End Date: $end_date\n" +
                "   Tag: $tag\n" +
                "   Completed: $is_complete\n"

    }
}





