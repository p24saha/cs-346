package model

import com.whatoodo.desktop.Components.ItemBox
import com.whatoodo.desktop.HelperFunctions.FileReaderHelper
import com.whatoodo.desktop.HelperFunctions.SettingsHelper
import com.whatoodo.desktop.Pages.*
import javafx.scene.Scene
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.stage.Stage


class Presenter(private val stage: Stage, model: Model) : IObserver {
    private val model: Model
    private var currentScene: Scene? = null
    private val DEFAULT_SETTINGS: SettingsModel = SettingsModel(800.0, 650.0, 560.0, 130.0, false)
    lateinit var settings: SettingsModel
    var width: Double
        get() = settings.width
        set(value) { settings.width = value }

    var height: Double
        get() = settings.height
        set(value) { settings.height = value }

    var x: Double
        get() = settings.x
        set(value) { settings.x = value }

    var y: Double
        get() = settings.y
        set(value) { settings.y = value }

    var darkmode: Boolean
        get() = settings.darkmode
        set(value) { settings.darkmode = value }

    val frh: FileReaderHelper = FileReaderHelper()

    // methods to manage data for the active view
    fun getValue(key: String?): String? {
        return model.getValue(key)
    }

    override fun update() {
        (currentScene as IObserver?)?.update()
    }

    fun addStylesheets() {
        // early in the bootup, can't apply stylesheets yet
        if (currentScene == null) {
            return
        }

        refreshColourStyles()
        if (darkmode) {
            currentScene!!.getStylesheets().add(frh.appendURL("styles/darkmodeStyles.css"))
        }   else {
            currentScene!!.getStylesheets().add(frh.appendURL("styles/styles.css"))
        }
    }

    fun refreshColourStyles() {
        if (darkmode) {
            currentScene!!.getStylesheets().remove(frh.appendURL("styles/styles.css"))
        } else {
            currentScene!!.getStylesheets().remove(frh.appendURL("styles/darkmodeStyles.css"))
        }
    }
    fun applySettings() {
        stage.height = settings.height
        stage.width = settings.width
        stage.x = settings.x
        stage.y = settings.y
        darkmode = settings.darkmode
        addStylesheets()
    }

    // methods to control which view is active and visible
    fun start() {

        // grab and set settings
        try {
            // try to apply styles from settings.json
            settings = SettingsHelper().settings
            applySettings()
        } catch(e: Exception) {
            // something failed, apply the default styling
            settings = DEFAULT_SETTINGS
            applySettings()
        }

        // Create starting scene
        if (currentScene == null) {
            currentScene = Login(this)
        }

        stage.scene = currentScene
        attachHotkeys()
        addStylesheets()
        stage.show()
    }

    fun refresh() {
        if (currentScene is View) {
            currentScene = View(this)
        }else if (currentScene is Today) {
            currentScene = Today(this)
        }else if (currentScene is Add) {
            currentScene = Add(this)
        }
        addStylesheets()
        stage.scene = currentScene

        attachHotkeys()
        stage.show()
    }

    fun advance() {
        // Create new scene
        if (currentScene is Login) {
            currentScene = View(this)
        } else if (currentScene is Signup) {
            currentScene = View(this)
        } else if (currentScene is Add) {
            currentScene = View(this)
        } else if (currentScene is Edit) {
            currentScene = View(this)
        }

        // Attach the scene to the stage and show it
        if (currentScene != null) {
            addStylesheets()
        }
        stage.scene = currentScene
        attachHotkeys()
        stage.show()
    }

    // overloaded advance function for Edit Items page, which allows for passing in existing item values
    fun advance(itemId: Long, name: String, group: String, shared: String, startDate: String, dueDate: String, starred: Boolean, priority: Int, done: Boolean) {
        if (currentScene is View) {
            currentScene = Edit(itemId, name, group, shared, startDate, dueDate, priority, starred, done,this)
        }

        // Attach the scene to the stage and show it
        if (currentScene != null) {
            addStylesheets()
        }
        stage.scene = currentScene
        attachHotkeys()
        stage.show()
    }

    fun rewind() {
        // Create new scene
        if (currentScene is Login) {
            currentScene = Signup(this)
        } else if (currentScene is View) {
            currentScene = Login(this)

        }

        // Attach the scene to the stage and show it
        if (currentScene != null) {
            addStylesheets()
        }
        stage.scene = currentScene
        stage.show()
    }

    fun add() {
        // Create new scene
        if (currentScene is Today || currentScene is View) {
            currentScene = Add( this)
        }

        // Attach the scene to the stage and show it
        if (currentScene != null) {
            addStylesheets()
        }
        stage.scene = currentScene
        stage.show()
    }

    fun view() {
        // Create new scene
        if ( currentScene is Add || currentScene is Today) {
            currentScene = View(this)
        }

        // Attach the scene to the stage and show it
        if (currentScene != null) {
            addStylesheets()
        }
        stage.scene = currentScene
        attachHotkeys()
        stage.show()
    }

    fun today() {
        // Create new scene
        if (currentScene is Add || currentScene is View) {
            currentScene = Today(this)
        }


        if (currentScene != null) {
            addStylesheets()
        }

        // Attach the scene to the stage and show it
        stage.scene = currentScene
        attachHotkeys()
        stage.show()
    }

    fun welcome() {
        // Create new scene

        currentScene = Login(this)

        if (currentScene != null) {
            addStylesheets()
        }

        // Attach the scene to the stage and show it
        stage.scene = currentScene
        stage.show()
    }

    fun undoAction() {
        if (currentScene is View) {
            (currentScene as View).undoLastAction()
        } else if (currentScene is Today) {
            (currentScene as Today).undoLastAction()
        }
    }

    fun redoAction() {
        if (currentScene is View) {
            (currentScene as View).redoLastAction()
        } else if (currentScene is Today) {
            (currentScene as Today).redoLastAction()
        }
    }

    fun copyAction() {
        if (currentScene is View) {
            (currentScene as View).copyItem()
        } else if (currentScene is Today) {
            (currentScene as Today).copyItem()
        }
    }
    fun pasteAction() {
        if (currentScene is View) {
            (currentScene as View).pasteItem()
        } else if (currentScene is Today) {
            (currentScene as Today).pasteItem()
        }
    }

    fun cutAction() {
        if (currentScene is View) {
            (currentScene as View).cutItem()
        } else if (currentScene is Today) {
            (currentScene as Today).cutItem()
        }
    }

    fun deleteItemBox(itemBox: ItemBox) {
        if (currentScene is View) {
            (currentScene as View).deleteItem(itemBox, oldIndex = ((currentScene as View).itemList.getIndexOf(itemBox)))
        }
    }

    private fun attachHotkeys() {

        fun updateStage() {
            stage.show()
        }

        val kcCopy: KeyCombination = KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN)
        val copyItemTask  = Runnable {
            copyAction()
        }
        val kcPaste: KeyCombination = KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN)
        val pasteItemTask  = Runnable {
            pasteAction()
            updateStage()
        }

        val kcCut: KeyCombination = KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN)
        val cutItemTask  = Runnable {
            cutAction()
            updateStage()
        }

        val kcUndo: KeyCombination = KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN)
        val undoItemTask  = Runnable {
            undoAction()
            updateStage()
        }

        val kcRedo: KeyCombination = KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN)
        val redoItemTask  = Runnable {
            redoAction()
            updateStage()
        }

        val kcUp: KeyCombination = KeyCodeCombination(KeyCode.U, KeyCombination.CONTROL_DOWN)
        val itemUp = Runnable {
            (currentScene as View).moveItemUp()

        }

        val kcDown: KeyCombination = KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN)
        val itemDown = Runnable {
            (currentScene as View).moveItemDown()
        }


        stage.scene.accelerators[kcCopy] = copyItemTask
        stage.scene.accelerators[kcPaste] = pasteItemTask
        stage.scene.accelerators[kcCut] = cutItemTask
        stage.scene.accelerators[kcUndo] = undoItemTask
        stage.scene.accelerators[kcRedo] = redoItemTask
        stage.scene.accelerators[kcUp] = itemUp
        stage.scene.accelerators[kcDown] = itemDown
    }

    init {
        stage.title = "WhatTooDo?"
        stage.isResizable = true

        stage.width = DEFAULT_SETTINGS.width
        stage.height = DEFAULT_SETTINGS.height


        // save window size & user settings (like colour mode)
        stage.setOnCloseRequest {
            try {
                // write settings to settings.json
                SettingsHelper().saveSettings(
                    SettingsModel(
                    stage.width,
                    stage.height,
                    stage.x,
                    stage.y,
                    darkmode
                    )
                )
            } catch (_: Exception) {
                // error handling, in-case the above fails we actually still want the window to close
            }

        }

        this.model = model
    }
}
