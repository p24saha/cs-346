import enum.Priority
import org.junit.jupiter.api.Test
import java.time.LocalDate
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

// TODO: Use dependency injection
class TodoListContextTest {
//    private var listNoName = TodoListContextDeprecated()
//    private var testName: String = "Test List"
//    private var testUserId = 0
//    private var testItem = TodoItemContext("")
//    private var testCategory = "testCategory"
//
//    private var sampleItem = TodoItemContext("sample")
//    private var sampleId = sampleItem.itemId
//
//    private var startDate: LocalDate = LocalDate.of(2023, 1, 1)
//    private var endDate: LocalDate = LocalDate.of(2023, 12, 30)
//    private var samplePriority: Priority = Priority.HIGH
//
//
//    @Test
//    fun `On init, list title has default name if no title is provided`() {
//        val listNoNameDummy = TodoListContextDeprecated()
//        assertEquals("List ${listNoNameDummy.listId}", listNoNameDummy.listName)
////    }
//
//    @Test
//    fun `Setting title with blank string sets default name with list ID`() {
//        listNoName.listName = ""
//        assertEquals("List ${listNoName.listId}", listNoName.listName)
//    }
//
//    @Test
//    fun `Multiple lists do not have the same ID`() {
//        val temporaryList = TodoListContextDeprecated()
//        assertNotEquals(temporaryList.listName, listNoName.listName)
//    }
//
//    @Test
//    fun `List initialized with a name should have its name set properly`() {
//        val listItemWithDefaultTitle = TodoItemContext(testName)
//        assertEquals(testName, listItemWithDefaultTitle.itemTitle)
//    }
//
//    @Test
//    fun `User's id is displayed in shared list once added, and not displayed once removed`() {
//        listNoName.addSharedUser(testUserId)
//        assertEquals(true, listNoName.shared.contains(testUserId))
//        listNoName.removeSharedUser(testUserId)
//        assertEquals(false, listNoName.shared.contains(testUserId))
//    }
//
//    @Test
//    fun `Item is successfully added and removed from TodoList`() {
//        listNoName.addItem(testItem)
//        assertEquals(true, listNoName.toDoList.contains(testItem))
//        listNoName.removeItem(testItem)
//        assertEquals(false, listNoName.toDoList.contains(testItem))
//    }
//
//    @Test
//    fun `Category is successfully added and removed from TodoList`() {
//        listNoName.addCategory(testCategory)
//        assertEquals(true, listNoName.categories.contains(testCategory))
//        listNoName.removeCategory(testCategory)
//        assertEquals(false, listNoName.categories.contains(testCategory))
//    }
//
//    @Test
//    fun `List should be successfully favourited and un-favourited`() {
//        listNoName.addFavourite()
//        assertEquals(true, listNoName.isFavourite)
//        listNoName.removeFavourite()
//        assertEquals(false, listNoName.isFavourite)
//    }
//
//    @Test
//    fun `List item title should be changed when modified`() {
//        listNoName.addItem(sampleItem)
//        listNoName.modifyItem("title", "New Title", sampleId)
//        assertEquals("New Title", sampleItem.itemTitle)
//    }
//
//    @Test
//    fun `List item tags can be added and removed` () {
//        listNoName.addItem(sampleItem)
//        listNoName.modifyItem("addTag", "testTag", sampleId)
//        assertEquals(true, sampleItem.tags.contains("testTag"))
//        listNoName.modifyItem("removeTag", "testTag", sampleId)
//        assertEquals(false, sampleItem.tags.contains("testTag"))
//    }
//
//    @Test
//    fun `List item flags can be added and removed` () {
//        listNoName.addItem(sampleItem)
//        listNoName.modifyItem("addFlag", "", sampleId)
//        assertEquals(true, sampleItem.isFlagged)
//        listNoName.modifyItem("removeFlag", "", sampleId)
//        assertEquals(false, sampleItem.isFlagged)
//    }
//
//    @Test
//    fun `List item end date and start date can be changed` () {
//        listNoName.addItem(sampleItem)
//        listNoName.modifyItem("changeEndDate", endDate, sampleId)
//        assertEquals(true, sampleItem.endDate == endDate)
//        listNoName.modifyItem("changeStartDate", startDate, sampleId)
//        assertEquals(true, sampleItem.startDate == startDate)
//    }
//
//    @Test
//    fun `List item priority can be changed` () {
//        listNoName.addItem(sampleItem)
//        listNoName.modifyItem("changePriority", samplePriority, sampleId)
//        assertEquals(true, sampleItem.priority == samplePriority)
//    }
//
//    @Test
//    fun `Duplicate items should have same fields as original item, but different id` () {
//        var newItem = listNoName.duplicateItem(sampleItem)
//        assertEquals(sampleItem.itemTitle, newItem.itemTitle)
//        assertEquals(sampleItem.startDate, newItem.startDate)
//        assertEquals(sampleItem.endDate, newItem.endDate)
//        assertEquals(sampleItem.isFlagged, newItem.isFlagged)
//        assertEquals(sampleItem.priority, newItem.priority)
//        assertEquals(sampleItem.tags, newItem.tags)
//        assertEquals(false, sampleItem.itemId == newItem.itemId)
//    }
}