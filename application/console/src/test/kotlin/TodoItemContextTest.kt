import org.junit.jupiter.api.Test
import java.time.LocalDate
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

// TODO: Use dependency injection
class TodoItemContextTest {
//    private val defaultTitle: String = "Placeholder List Item Name"
//    private val listItemNoName: TodoItemContext = TodoItemContext("")
//
//    @Test
//    fun `On init, item title has default name if no title is provided`() {
//        val listItemNoNameDummy = TodoItemContext("")
//        assertEquals("Item ${listItemNoNameDummy.itemId}", listItemNoNameDummy.itemTitle)
//    }
//    @Test
//    fun `Setting title with blank string sets default name with item ID`() {
//        listItemNoName.itemTitle = ""
//        assertEquals("Item ${listItemNoName.itemId}", listItemNoName.itemTitle)
//    }
//
//    @Test
//    fun `Multiple list items do not have the same ID`() {
//        val temporaryListItem = TodoItemContext("")
//        assertNotEquals(temporaryListItem.itemTitle, listItemNoName.itemTitle)
//    }
//
//    @Test
//    fun `Setting end date to a valid date works as expected`() {
//        val tmrw = LocalDate.now().plusDays(1)
//        listItemNoName.endDate = tmrw
//        assertEquals(tmrw, listItemNoName.endDate)
//    }
//
//    @Test
//    fun `Setting start date to a valid date works as expected`() {
//        // This test relies on the end date setter working properly for valid dates
//        val tmrw = LocalDate.now().plusDays(1)
//        listItemNoName.endDate = tmrw.plusDays(2)
//        listItemNoName.startDate =  tmrw// set date to tomorrow
//        assertEquals(tmrw, listItemNoName.startDate)
//    }
//
//    @Test
//    fun `Start date set to a date before today does not change the start date`() {
//        // this test relies on the endDate and startDate setters working properly for valid dates
//        val tmrw = LocalDate.now().plusDays(1); // tomorrow's date
//        listItemNoName.endDate = tmrw.plusDays(2) // MUST set date properly
//        listItemNoName.startDate = tmrw // MUST set date properly
//        listItemNoName.startDate = LocalDate.of(2000, 1, 1) // should not work
//        assertEquals(tmrw, listItemNoName.startDate)
//    }
//
//    @Test
//    fun `Start date set to a date before the end date is unchanged`() {
//        // should we still use date objects
//        listItemNoName.endDate = LocalDate.of(2000, 1, 1)
//        assertEquals(listItemNoName.startDate, listItemNoName.endDate)
//    }
//
//    @Test
//    fun `End date set to a date before Start date sets End date to Start date`() {
//        // should we still use date objects
//        listItemNoName.endDate = LocalDate.of(2000, 1, 1)
//        assertEquals(listItemNoName.startDate, listItemNoName.endDate)
//    }
//
//    @Test
//    fun `List item initialized with a name should have its name set properly`() {
//        val listItemWithDefaultTitle = TodoItemContext(defaultTitle)
//        assertEquals(defaultTitle, listItemWithDefaultTitle.itemTitle)
//    }
}