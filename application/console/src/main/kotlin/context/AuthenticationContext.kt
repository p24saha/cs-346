package context

import com.google.gson.Gson
import model.Message
import model.User
import payload.LoginPayload
import payload.RegistrationPayload
import java.net.URI
import java.net.URL
import java.net.URLConnection
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class AuthenticationContext {

    var server_address: String = "http://localhost:8000"
    var cookie = ""

    // POST
    fun login(payload: LoginPayload): Message {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/user-auth/login"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        val responseBody = gson.fromJson(response.body(), Message::class.java)
       // println(response.body())
        if (responseBody.error_code.equals("LOGIN_SUCCESS")) {
            cookie = response.headers().allValues("set-cookie")[0]
        }

        return responseBody
    }

    //POST
    fun registration(payload: RegistrationPayload): Message {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/user-auth/register"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), Message::class.java)
    }

    // POST
    fun logout(): Message {
        var gson = Gson()

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/user-auth/logout"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(""))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), Message::class.java)
    }


    fun getUser(): User {
        var gson = Gson()
        // var cookie = HttpCookie("jwt", "eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiI0MiIsImV4cCI6MTY2OTk2NjA2M30.zHdWvqhu9ITeVLSmj69MnwjXcL9C0pshR1bXbI-f3-ny2BUk5yTz-XsIH5a_WM93EAazh2FosHFeUOvvaHiq4A" )
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/user-auth/user"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .setHeader("Cookie", cookie)
            .GET()
            .build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), User::class.java)

    }
}
