package context

import com.google.gson.Gson
import model.TodoList
import model.User
import payload.TodoListPayload
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.util.Locale.Category

class TodoListContext {
    /**
     * current_list
     * add_list
     * edit_list
     * remove_list
     */
    var server_address: String = "http://localhost:8000"

    fun getList(): Array<TodoList>? {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson: Gson = Gson()
        var lists: Array<TodoList>? = gson.fromJson(response.body(), Array<TodoList>::class.java)
        // println(lists.toString())

        return lists
    }

    fun getListById(listId: Long): TodoList {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list/$listId"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson = Gson()
        //        println(list.toString())

        return gson.fromJson(response.body(), TodoList::class.java)
    }

    fun addList(payload: TodoListPayload): TodoList {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), TodoList::class.java)
    }

    fun addUserToList(userid: Long, listId: Long): User {
        var gson = Gson()

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list/$listId/user/$userid"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .PUT(HttpRequest.BodyPublishers.ofString(""))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), User::class.java)
    }

    fun editList(listId: Long, payload: TodoListPayload): TodoList {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list/$listId"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .PUT(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), TodoList::class.java)

    }

    fun deleteList(listId: Long): Long {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-list/$listId"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).DELETE().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString()).body()
        return listId
    }


}