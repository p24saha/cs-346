package context

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import model.TodoItem
import model.TodoList
import payload.TodoItemPayload
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class TodoItemContext {
    var server_address: String = "http://localhost:8000"
    fun getItems(): Array<TodoItem>? {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-item"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson = Gson()
        var items: Array<TodoItem>? = gson.fromJson(response.body(), Array<TodoItem>::class.java)
        //println(items.toString())

        return items

    }

    fun getItemById(itemId: Long): TodoItem? {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-item/$itemId"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson = Gson()
        //        println(list.toString())


        return gson.fromJson(response.body(), TodoItem::class.java)

    }

    fun addItem(payload: TodoItemPayload): TodoItem {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        //println("pay:" + requestBody)
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-item"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // println(response.body())
        return gson.fromJson(response.body(), TodoItem::class.java)
    }

    fun deleteItem(itemId: Long): String {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-item/$itemId"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).DELETE().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString()).body()
        return response + "with id: $itemId"
    }

    fun editItem(itemId: Long, payload: TodoItemPayload): TodoItem {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)

        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/todo-item/$itemId"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .PUT(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), TodoItem::class.java)
    }

    fun addListToItem(itemId: Long, listId: Long): TodoList {
        var gson = Gson()
        // var requestBody: String = gson.toJson(payload)
        val client = HttpClient.newBuilder().build()
        // note: my listId is 1, may be different for you
        val endpoint = "$server_address/todo-item/$itemId/todo-list/$listId"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .PUT(HttpRequest.BodyPublishers.ofString(""))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), TodoList::class.java)
    }

    fun filterByEndDate(listId: Long, date: String): Array<TodoItem>? {
        val client = HttpClient.newBuilder().build()
        //http://localhost:8000/todo-item/filter-end-date?date=2022-11-27&list_id=2
        val endpoint = "$server_address/todo-item/filter-end-date?date=2022-11-27&list_id=${listId}"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson = Gson()
        //        println(list.toString())

        return gson.fromJson(response.body(), Array<TodoItem>::class.java)
    }
}