package context

import com.google.gson.Gson
import model.TodoItem
import model.User
import payload.UserPayload
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.util.LongSummaryStatistics

class UserContext {
    /**
     * println("User Menu Commands:")
    println("[user-details]         Show all user details")
    println("[reset-password]       Reset Password")
    println("[change-username]      Change Username")
    println("[change-firstname]     Change Firstname")
    println("[change-lastname]      Change Lastname")
    println("[change-occupation]    Change Occupation\n")
     *
     */

    var server_address: String = "http://localhost:8000"

    fun getUserbyId(id: Long): User? {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/users/${id}"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).GET().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        // converting string to json to object
        var gson = Gson()
        //        println(list.toString())

        return gson.fromJson(response.body(), User::class.java)
    }

    fun updateUser(payload: UserPayload, id: Long): User? {
        var gson = Gson()
        var requestBody: String = gson.toJson(payload)
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/users/$id"
        val request = HttpRequest.newBuilder()
            .uri(URI.create(endpoint))
            .header("Content-Type", "application/json")
            .PUT(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        return gson.fromJson(response.body(), User::class.java)
    }

    fun deleteUser(id: Long): String {
        val client = HttpClient.newBuilder().build()
        val endpoint = "$server_address/users/$id"
        val request = HttpRequest.newBuilder().uri(URI.create(endpoint)).DELETE().build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString()).body()
        return response + "with id: $id"
    }

}