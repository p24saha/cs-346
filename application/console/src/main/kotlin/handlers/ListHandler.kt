package handlers

import context.TodoListContext
import model.TodoList
import model.User
import payload.TodoListPayload
import payload.UserPayload

class ListHandler(private val listContext: TodoListContext) {
    /**
     *  println("Todo List commands:")
    println("[current-list]         Displays current list")
    println("[add-list]             Add a new list")
    println("[remove-list]          Remove a list")
    println("[edit-list]            Modify current list")
    println("[copy-list]            Copy current list")
    println("[paste-list]           Pastes a list into current list")
    println("[duplicate-list]       Duplicates current list")
    println("[switch-list]          Changes current list into an another existing list")
     */

    fun getAllLists() {

    }
    fun currentList(id: Long): Long {
        // get the current list

        var list: TodoList = listContext.getListById(id)
        // print current list
        println(list)


        var currentListId: Long = list.list_id
        return currentListId
    }

    fun addList(userid: Long): Long {
        // create payload
        var listname: String = ""
        var category: String = ""
        var isFavourite: String = ""
        var isFav: Boolean = false

        print("List name:")
        listname = readln().toString()
        println()
        print("Category:")
        category = readln().toString()
        println()
        print("Favourite this list? [yes/no]: ")
        isFavourite = readln().toString()
        if ( listname.isNullOrEmpty() || category.isNullOrEmpty()) {
            println("Listname and category are required")
        } else {
            if (isFavourite.equals("yes")) {
                isFav = true
            } else if (isFavourite.equals("no") || isFavourite.isNullOrEmpty()) {
                isFav = false
            }
            // create paylod
            var payload: TodoListPayload = TodoListPayload(listname, category, isFav)
            // add the list
            var list: TodoList = listContext.addList(payload)
            //println(list)
            // add the user to the list
            var user: User = listContext.addUserToList(userid, list.list_id)
            return user.userid
        }
        return 0;
    }

    fun removeList(): Long {
        var id: Long;
        print("List to remove <list-id>:")
        id = readln().toLong()
        println()
        var delete: Long = listContext.deleteList(id)
        return delete
    }

    fun changeListname(listid: Long): TodoList {
        var listname: String = ""
        var editedList: TodoList;
        print("New List Name:")
        listname = readln().toString()
        println()
        if (listname.isNullOrEmpty()) {
            println("Lastname is required.")
        } else {
            var list = listContext.getListById(listid)
            var payload: TodoListPayload = TodoListPayload(listname, list.category, list.is_favourite)
            editedList = listContext.editList(listid, payload)
            println("List name updated successfully")
            // print(editedList)
            return editedList
        }

        return listContext.getListById(listid)
    }

    fun changeCategory(listid: Long): TodoList {
        var category: String = ""
        var editedList: TodoList;
        print("New Category:")
        category = readln().toString()
        println()
        if (category.isNullOrEmpty()) {
            println("Category is required.")
        } else {
            var list = listContext.getListById(listid)
            var payload: TodoListPayload = TodoListPayload(list.list_name, category, list.is_favourite)
            editedList = listContext.editList(listid, payload)
            println("Category updated successfully")
            // print(editedList)
            return editedList
        }

        return listContext.getListById(listid)

    }

    fun favouriteList(listid: Long): TodoList {
        var isFavorite: String = ""
        var isFav: Boolean = false
        var editedList: TodoList;
        print("Favourite List ? [yes/no]:")
        isFavorite = readln().toString()
        println()
        if (isFavorite.isNullOrEmpty()) {
            println("Category is required.")
        } else {
            if (isFavorite.isNullOrEmpty() || isFavorite.equals("no")) {
                isFav = false
            } else if (isFavorite.equals("yes")) {
                isFav = true
            }
            var list = listContext.getListById(listid)
            var payload: TodoListPayload = TodoListPayload(list.list_name, list.category, isFav)
            editedList = listContext.editList(listid, payload)
            println("List Favourite updated successfully")
            // print(editedList)
            return editedList
        }

        return listContext.getListById(listid)
    }

    fun copyList(userid: Long): TodoList {
        // get the list id to copy
        var copyid: Long
        print("Enter the list ID to copy:")
        copyid = readln().toLong()
        // get that list - check the list exists
        var copyList: TodoList = listContext.getListById(copyid)

        println("List Copied Successfully.")
        return copyList
    }

    fun pasteList(userid: Long, copyList: TodoList) {
        // create payload
        var payload: TodoListPayload = TodoListPayload(copyList.list_name, copyList.category, copyList.is_favourite)
        // add the list
        var pastedList: TodoList = listContext.addList(payload)
        // add the user to the list
        var user: User = listContext.addUserToList(userid, pastedList.list_id)
        //println(user)
        // print the list
        println("Pasted the list successfully.")
        // println(user.printAll())
        //println(user.todo_lists.find { it.list_id == pastedList.list_id }.toString())

    }

    fun duplicateList(userid: Long) {
        var duplicateId: Long
        print("Enter the list ID to duplicate:")
        duplicateId = readln().toLong()
        println()

        var copyList: TodoList = listContext.getListById(duplicateId)
        var payload: TodoListPayload = TodoListPayload(copyList.list_name, copyList.category, copyList.is_favourite)
        // add the list
        var pastedList: TodoList = listContext.addList(payload)
        // add the user to the list
        var user: User = listContext.addUserToList(userid, pastedList.list_id)
        // print the list
        println("Duplicated list successfully.")
        // user.todo_lists.find { it.list_id == pastedList.list_id }?.let { println(it.toString()) }

    }

    fun switchList(currentId: Long): Long {
        println("Your current list ID: $currentId")
        var newid: Long
        println("Enter the new list ID you want to switch to:")
        newid = readln().toLong()
        return newid;
    }
}