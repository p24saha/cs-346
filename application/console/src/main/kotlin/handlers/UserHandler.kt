package handlers

import context.UserContext
import model.User
import payload.UserPayload

class UserHandler(private val userContext: UserContext) {
    /**
     * println("User Menu Commands:")
    println("[user-details]         Show all user details")
    println("[reset-password]       Reset Password")
    println("[change-username]      Change Username")
    println("[change-firstname]     Change Firstname")
    println("[change-lastname]      Change Lastname")
    println("[change-occupation]    Change Occupation\n")
     *
     */

    fun userDetails(id: Long) {
        println(userContext.getUserbyId(id))
    }

    fun resetPassword(user: User) {
        var newPassword: String = ""
        print("Enter your new password:")
        newPassword = readln().toString()
        if (newPassword.isNullOrEmpty()) {
            println("Password is required.Try again.")
        } else {
            var payload: UserPayload = UserPayload(user.username, user.email, newPassword, user.firstname, user.lastname, user.occupation)
            var user: User? = userContext.updateUser(payload, user.userid)
            println("Password reset successful")
            println(user)
        }
    }

    fun changeUsername(user: User) {
        var newUsername: String = ""
        print("Enter your new username:")
        newUsername = readln().toString()
        if (newUsername.isNullOrEmpty()) {
            println("Username is required.Try again.")
        } else {
            var payload: UserPayload = UserPayload(newUsername, user.email, user.password, user.firstname, user.lastname, user.occupation)
            var user: User? = userContext.updateUser(payload, user.userid)
            println("Username updated successfully.")
            println(user)
        }
    }

    fun changeFirstname(user: User) {

        var newFirstname: String = ""
        print("Enter your new firstname:")
        newFirstname = readln().toString()
        if (newFirstname.isNullOrEmpty()) {
            println("Username is required.Try again.")
        } else {
            var payload: UserPayload = UserPayload(user.username, user.email, user.password, newFirstname, user.lastname, user.occupation)
            var user: User? = userContext.updateUser(payload, user.userid)
            println("Firstname updated successfully")
            println(user)
        }
    }

    fun changeLastName(user: User) {
        var newLastname: String = ""
        print("Enter your new lastname:")
        newLastname = readln().toString()
        if (newLastname.isNullOrEmpty()) {
            println("Username is required.Try again.")
        } else {
            var payload: UserPayload = UserPayload(user.username, user.email, user.password, user.firstname, newLastname, user.occupation)
            var user: User? = userContext.updateUser(payload, user.userid)
            println("Lastname updated successfully")
            println(user)
        }
    }

    fun changeOccuptation(user: User) {
        var newOccupation: String = ""
        print("Enter your new occupation:")
        newOccupation = readln().toString()
        if (newOccupation.isNullOrEmpty()) {
            println("Occupation is required.Try again.")
        } else {
            var payload: UserPayload = UserPayload(user.username, user.email, user.password, user.firstname, user.lastname, newOccupation)
            var user: User? = userContext.updateUser(payload, user.userid)
            println("Occupation updated successfully")
            println(user)
        }
    }

    fun deleteUser(id: Long) {
        var message = userContext.deleteUser(id)
        println(message)
    }
}