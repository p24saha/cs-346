package handlers

import context.AuthenticationContext
import model.Message
import model.User
import payload.LoginPayload
import payload.RegistrationPayload

class AuthenticationHandler(private val authenticationContext: AuthenticationContext) {
     var user: User = authenticationContext.getUser()
    fun logout() {
        var message = authenticationContext.logout()
        println(message)
    }

    fun isLoggedin(): Boolean{
        var user: User = authenticationContext.getUser()
        //println(authenticationContext.cookie)
        var id: Int = 0
        if (user.userid != id.toLong()) {
            return true
        }
        return false
    }

    fun login(): User {
        var email: String = ""
        var password: String = ""

        println("===============================================================")
        println("LOGIN:")
        println("===============================================================")

        print("Email:")
        email = readln().toString();
        println("Password:")
        password = readln().toString();

        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            println("Email and password are required. Try again.")
        } else {
            // login
            var payload: LoginPayload = LoginPayload(email, password)
            var message: Message = authenticationContext.login(payload)
            if (message.error_code.equals("LOGIN_SUCCESS")) {
                user = authenticationContext.getUser()
            }
            println(message)
        }
        return user
    }

    fun register(): User {
        var username: String = ""
        var email: String = ""
        var password: String = ""
        var firstname: String = ""
        var lastname: String = ""
        var occupation: String = ""
        println("===============================================================")
        println("REGISTRATION:")
        println("===============================================================")
        print("Username:")
        username = readln().toString()
        println()
        print("Email:")
        email = readln().toString()
        println()
        print("Password:")
        password = readln().toString()
        println()
        print("Firstname:")
        firstname = readln().toString()
        println()
        print("Lastname:")
        lastname = readln().toString()
        println()
        print("Occupation:")
        occupation = readln().toString()
        println()

        if (email.isNullOrEmpty() || password.isNullOrEmpty() || username.isNullOrEmpty()) {
            println("Username, Email and password are required. Try again.")
        } else {
            // login
            var payload: RegistrationPayload= RegistrationPayload(username, email, password, firstname, lastname, occupation)
            // println(payload)
            var message: Message = authenticationContext.registration(payload)

//            println(message)
            if (message.error_code.equals("REGISTRATION_SUCCESS")) {
                user = authenticationContext.getUser()
                println("To continue login again.")
                login()
            }

        }
        return user
    }

}