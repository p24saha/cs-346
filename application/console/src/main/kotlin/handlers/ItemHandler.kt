package handlers

import context.TodoItemContext
import model.TodoItem
import model.TodoList
import payload.TodoItemPayload
import java.sql.Timestamp
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*


class ItemHandler(private val itemContext: TodoItemContext) {
    /**
     * println("Todo Item commands:")
    println("[add-item]             Add new item to current list")
    println("[edit-item]            Edit item on current list")
    println("[remove-item]          Remove item from current list")
    println("[duplicate-item]       Duplicate item in current list")
    println("[copy-item]            Copy item from current list")
    println("[paste-item]           Paste item into current list")
    println("[flag-item]            Flag item in current list")
   - println("[undo_item]            Undo action on item")
    - println("[redo-item]            Redo action on item")
     *
     */

    val formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val formatTime = DateTimeFormatter.ofPattern("hh:mm:ss")
    fun addItem(currentListId: Long) {
        // create payload
        var item_title: String  = ""
        var item_description: String = ""
        var priority: Int = 2
        var is_flagged: Boolean = false
        var start_date: String = ""
        var end_date: String = ""
        var tag: String = ""
        var is_complete: Boolean = false

        println("Enter item title:")
        item_title = readln().toString()
        println()

        println("Enter item description:")
        item_description = readln().toString()
        println()

        println("Enter priority [HIGH = 0; MEDIUM = 1, LOW = 2]:")
        priority = readln().toInt()
        println()

        println("Flag the item? [yes/no] :")
        var flag = readln().toString()
        println()
        if (flag.isNullOrEmpty() || flag.equals("no")) {
            is_flagged = false
        } else if (flag.equals("yes")) {
            is_flagged = true
        }

        println("Start Date [yyyy-MM-dd]:")
        start_date= readln().toString()
        //var date: LocalDateTime = LocalDateTime()
        start_date +=  " 05:00:00"
        println()

        println("End Date [yyyy-MM-dd]:")
        end_date = readln().toString()
        //var date: LocalDateTime = LocalDateTime()
        end_date += " 05:00:00"

        println("Enter a tag:")
        tag = readln().toString()
        println()

        println("Completed? [yes/no] :")
        var completed = readln().toString()
        println()
        if (completed.isNullOrEmpty() || completed.equals("no")) {
            is_complete = false
        } else if (flag.equals("yes")) {
            is_complete = true
        }

        // create payload
        var payload: TodoItemPayload = TodoItemPayload(item_title, item_description, priority, is_flagged, start_date, end_date, tag, is_complete)
        var item: TodoItem = itemContext.addItem(payload)

        // add the item to the current list
        var list: TodoList = itemContext.addListToItem(item.item_id, currentListId)
        //println(item)
    }


    fun editItem() {

        print("Enter the ID of the item you want to edit:")
        var itemId: Long = readln().toLong()
        println()

        print("Enter the field you want to edit [title, description, priority, flag, start-date, end-date, tag, completed]:")
        var field: String = readln().toString()
        println()
        // get the item

        try {
            var item: TodoItem? = itemContext.getItemById(itemId)
            var title: String? = item?.item_title
            var description: String? = item?.item_description
            var priority: Int? = item?.priority?.priorityAsNum
            var flag: Boolean? = item?.is_flagged
            var startDate: String? = item?.start_date
            var endDate: String? = item?.end_date
            var tag: String? =  item?.tag
            var completed: Boolean? = item?.is_complete
            when(field) {
                "title" -> {
                    print("Enter new item title: ")
                    title = readln().toString()
                    println()
                }

                "description" -> {
                    print("Enter new item description:")
                    description = readln().toString()
                    println()
                }
                "priority" -> {
                    print("Enter new item priority [HIGH = 0, MEDIUM = 1, LOW = 2]:")
                    priority = readln().toInt()
                    println()
                }
                "flag" -> {
                    print("Flag item? [yes/no]:")
                    var flagged = readln().toString()
                    if (flagged.isNullOrEmpty() || flagged.equals("no")) {
                        flag = false
                    } else if (flagged.equals("yes")) {
                        flag = true
                    }
                    println()
                }
                "start-date" -> {
                    print("Enter new item start-date [YYYY-MM-DD]:")
                    startDate = readln().toString()
                    startDate +=  " 05:00:00"
                    println()
                }
                "end-date" -> {
                    print("Enter new item end-date [YYYY-MM-DD]:")
                    endDate = readln().toString()
                    endDate +=  " 05:00:00"
                    println()
                }
                "tag" -> {
                    print("Enter new item tag:")
                    tag = readln().toString()
                    println()
                }
                "completed" -> {
                    print("Completed item? [yes/no]:")
                    var complete = readln().toString()
                    if (complete.isNullOrEmpty() || complete == "no") {
                        completed = false
                    } else if (complete == "yes") {
                        completed = true
                    }
                    println()
                }
                else -> {
                    println("Invalid command. Please try again.")
                }
            }
            // create payload
            var payload: TodoItemPayload = TodoItemPayload(title, description, priority, flag, startDate, endDate, tag, completed)
            var updateItem: TodoItem = itemContext.editItem(itemId, payload)
            println("Item updated successfully.")
            // println(updateItem)
        } catch (e: Exception) {
            throw Exception ("Item does not exist. Please try again")
        }
    }

    fun removeItem(): Long{
        var remove: Long = 0
        print("Enter the Item ID to remove:")
        remove = readln().toLong()
        println()

        var message = itemContext.deleteItem(remove)
        println(message)
        return remove
    }

    fun duplicateItem(listid: Long) {
        var duplicate: Long = 0
        print("Enter the Item ID to duplicate:")
        duplicate = readln().toLong()
        println()

        var copy: TodoItem? = itemContext.getItemById(duplicate)




        var payload: TodoItemPayload = TodoItemPayload(copy?.item_title,
            copy?.item_description, copy?.priority?.priorityAsNum, copy?.is_flagged,  copy?.start_date,
            copy?.end_date, copy?.tag, copy?.is_complete)

        var item: TodoItem = itemContext.addItem(payload)
        var list: TodoList = itemContext.addListToItem(item.item_id, listid)
    }

    fun copyItem(): TodoItem? {
        var duplicate: Long = 0
        print("Enter the Item ID to copy:")
        duplicate = readln().toLong()
        println()
        var copy: TodoItem? = itemContext.getItemById(duplicate)
        println("Item Copied successfully.")
        // println(copy)
        return copy
    }

    fun pasteItem(copy: TodoItem, listid: Long) {


        var payload: TodoItemPayload = TodoItemPayload(copy?.item_title,
            copy?.item_description, copy?.priority?.priorityAsNum, copy?.is_flagged,  copy?.start_date,
            copy?.end_date, copy?.tag, copy?.is_complete)

        var item: TodoItem = itemContext.addItem(payload)
        var list: TodoList = itemContext.addListToItem(item.item_id, listid)
       // println("Pasted the item successfully.")
        // println(list.todo_items.find { it.item_id == item.item_id }.toString())
    }

    fun flagItem() {
        var is_flagged: Boolean = false
        var id: Long
        println("Enter the item id you want to flag:")
        id= readln().toLong()
        println()
        println("Flag the item? [yes/no] :")
        var flag = readln().toString()
        println()
        if (flag.isNullOrEmpty() || flag.equals("no")) {
            is_flagged = false
        } else if (flag.equals("yes")) {
            is_flagged = true
        }

        var copy: TodoItem? = itemContext.getItemById(id)
        //val startDate = LocalDateTime.ofInstant(copy?.start_date?.let { Instant.ofEpochMilli(it.time) }, ZoneId.systemDefault())
        //val endDate = LocalDateTime.ofInstant(copy?.end_date?.let { Instant.ofEpochMilli(it.time) }, ZoneId.systemDefault())

        var payload: TodoItemPayload = TodoItemPayload(copy?.item_title,
            copy?.item_description, copy?.priority?.priorityAsNum, copy?.is_flagged,  copy?.start_date,
            copy?.end_date, copy?.tag, copy?.is_complete)
        var item: TodoItem = itemContext.editItem(id, payload)


    }
}