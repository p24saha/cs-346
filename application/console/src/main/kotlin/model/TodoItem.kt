package model

import com.google.gson.annotations.Expose
import enum.Priority
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.Date

data class TodoItem (
    @Expose(serialize = false, deserialize = true)
    var item_id: Long,
    @Expose(serialize = true, deserialize = true)
    var item_title: String,
    @Expose(serialize = true, deserialize = true)
    var item_description: String,
    @Expose(serialize = true, deserialize = true)
    var priority: Priority,
    @Expose(serialize = true, deserialize = true)
    var is_flagged: Boolean,
    @Expose(serialize = true, deserialize = true)
    var start_date: String,
    @Expose(serialize = true, deserialize = true)
    var end_date: String,
    @Expose(serialize = true, deserialize = true)
    var tag: String,
    @Expose(serialize = true, deserialize = true)
    var is_complete: Boolean
) {
    override fun toString(): String {
        var response = ""
        var format: String = "| %-8d | %-9c | %-15s | %-20s | %-8s | %-20s | %-20s | %-8b | %-10s |"
        var completed: Char;
        if (is_complete) {
            completed = '\u2713'
        } else {
            completed = 'x'
        }
        response = format.format(item_id.toInt(), completed,item_title, item_description, priority, start_date, end_date, is_flagged, tag )
        return response

    }
}
