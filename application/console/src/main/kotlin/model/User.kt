package model


data class User(
    var userid: Long,
    var username: String,
    var email: String,
    var password: String,
    var firstname: String?,
    var lastname: String?,
    var occupation: String?,
    var todo_lists: MutableList<TodoList> = mutableListOf<TodoList>()
){

    override fun toString(): String {
        return (
                "----------------------------\n" +
                "User Details: $userid\n" +
                "----------------------------\n" +
                 "Username: $username\n" +
                 "Email: $email\n" +
                 "Firstname: $firstname\n" +
                 "Lastname: $lastname\n" +
                  "Occupation: $occupation\n" +
                 "----------------------------\n"
                )
    }

    fun printAll() {
        for (list in todo_lists) {
            println(list)
        }
    }


}