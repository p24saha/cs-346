package model

data class Message(
    val error_code: String,
    val error_message: String
) {
    override fun toString(): String {
        return "--------------------------------------------------------\n" +
        " $error_code : $error_message\n" +
        "--------------------------------------------------------\n"
    }
}
