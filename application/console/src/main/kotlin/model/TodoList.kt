package model

import com.google.gson.annotations.Expose

class TodoList {
    @Expose(serialize = false, deserialize = true)
    var list_id: Long = 0
    @Expose(serialize = true, deserialize = true)
    var list_name: String = ""
    @Expose(serialize = true, deserialize = true)
    var category: String = ""
    @Expose(serialize = true, deserialize = true)
    var is_favourite: Boolean = false
    @Expose(serialize = false, deserialize = true)
    var todo_items: MutableList<TodoItem> = mutableListOf<TodoItem>()

    constructor(listname: String, category: String, isFavourite: Boolean) : this() {
        this.list_name = listname
        this.category = category
        this.is_favourite = isFavourite
    }

    constructor()


    override fun toString(): String {

        var response =
            "========================================================================================================================================================\n" +
            "\t\t\tList ID: ${list_id}\n" +
            "\t\t\tList Name: $list_name\n" +
            "\t\t\tCategory: ${category}\n" +
            "\t\t\tFavourite: ${is_favourite}\n" +
            "========================================================================================================================================================\n"
        response +=
            "---------------------------------------------------------------------------------------------------------------------------------------------------\n"
        if (todo_items.size == 0) {
            response +=
                "---------------------------------------------------------------------------------------------------------------------------------------------------\n"
            response += "\t\t\tNo Items\n"
            response +=  "---------------------------------------------------------------------------------------------------------------------------------------------------\n"

        } else {
            // completed, title, description, priority, start date, end date, flagged, tag

            var format: String = "| %-8s | %-9s | %-15s | %-20s | %-8s | %-20s | %-20s | %-8s | %-10s |%n"
            response += format.format(
                "Item ID",
                "Completed",
                "Title",
                "Description",
                "Priority",
                "Start Date",
                "End Date",
                "Flagged",
                "Tag"
            )
            response +=
                "---------------------------------------------------------------------------------------------------------------------------------------------------\n"
            for(item in todo_items) {
                response = response + item.toString() + "\n"
                response +=
                    "---------------------------------------------------------------------------------------------------------------------------------------------------\n"
            }

        }


        return response

    }
}
