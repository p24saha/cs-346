package payload

data class LoginPayload(
    var email: String,
    var password: String
)
