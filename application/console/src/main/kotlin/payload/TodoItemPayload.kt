package payload

import enum.Priority
import java.util.Date

data class TodoItemPayload(
    var item_title: String?,
    var item_description: String?,
    var priority: Int?,
    var is_flagged: Boolean?,
    var start_date: String?,
    var end_date: String?,
    var tag: String?,
    var is_complete: Boolean?
)
