package payload

data class UserPayload (
    var username: String,
    var email: String,
    var password: String,
    var firstname: String?,
    var lastname: String?,
    var occupation: String?
) {
    override fun toString(): String {
        return "UserPayload(username='$username', email='$email', password='$password', firstname=$firstname, lastname=$lastname, occupation=$occupation)"
    }
}