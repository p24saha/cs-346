package payload

import java.util.Locale.Category

data class TodoListPayload(
    var list_name: String,
    var category: String,
    var is_favourite: Boolean
)
