package enum

enum class SortDirection {
    ASC,
    DESC
}