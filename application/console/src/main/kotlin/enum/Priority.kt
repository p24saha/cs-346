package enum

enum class Priority(val priorityAsNum: Int) {
    HIGH(0),
    MEDIUM(1),
    LOW(2),
    NONE(3)
}