import context.AuthenticationContext
import context.TodoItemContext
import context.TodoListContext
import context.UserContext
import enum.Priority
import handlers.AuthenticationHandler
import handlers.ItemHandler
import handlers.ListHandler
import handlers.UserHandler
import model.TodoItem
import model.TodoList
import model.User
import payload.LoginPayload
import payload.RegistrationPayload
import payload.TodoItemPayload
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

var userid: Long = 0;
lateinit var user: User
var currentListId: Long = 0;
var authenticationContext: AuthenticationContext = AuthenticationContext()
var userContext: UserContext = UserContext()
var itemContext: TodoItemContext = TodoItemContext()
var listContext: TodoListContext = TodoListContext()
var listHandler: ListHandler = ListHandler(listContext)
var authenticationHandler: AuthenticationHandler = AuthenticationHandler(authenticationContext)
var itemHandler: ItemHandler = ItemHandler(itemContext)
var userHandler: UserHandler = UserHandler(userContext)
var copyList: TodoList = TodoList()
lateinit var copyItem: TodoItem
fun main(args: Array<String>) {


//
//    // Getting command from cmd line
//    var listContext: TodoListContext = TodoListContext()
//    var response = listContext.getList()
//    if (response != null) {
//        for (list in response) {
//            println(list)
//
//        }
//    }
////    var listbl: TodoList = listContext.getListById(22);
////    // listbl.todo_items.get(0).item_description;
////    for(item in listbl.todo_items) {
////        println(item.item_id)
////    }
//    //println(listContext.getListById(2).toString())
//   // println(listContext.addList("Creating a new list test", "Test", true))
//    // println(listContext.deleteList(6))
//   //println("Edit list")
//    /println(listContext.editList(7, "New List1", "Test1", false).toString())
//
    var itemContext: TodoItemContext = TodoItemContext()
//    var response = itemContext.getItems()
//    if (response != null) {
//        for (item in response) {
//            println(item)
//
//        }
//    }
//    val myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd")
//    val myFormatObj2 = DateTimeFormatter.ofPattern("HH:mm:ss")
////   val dateFormat: DateTimeFormatter = DateTimeFormatter("yyyy-mm-dd hh:mm:ss")
////    val dateFormat2: DateFormat = SimpleDateFormat("yyyy-MM-ddThh:mm:ss")
//    var date: LocalDateTime = LocalDateTime.now()
////    println(dateFormat2.format(date).toString())
////    println(myFormatObj.format(date).toString())
//   //println(myFormatObj.format(date))
//   // println(myFormatObj2.format(date).toString())
//    //println(itemContext.getItemById(1))
// var payload: TodoItemPayload = TodoItemPayload(
//     "Item 6715367",
//     "jhasgdjhasgdahjsdt",
//     Priority.MEDIUM.priorityAsNum,
//     true,
//     myFormatObj.format(date) + "T" + myFormatObj2.format(date),
//     myFormatObj.format(date) + "T" + myFormatObj2.format(date),
//     "priya",
//     false)
// var newItem: TodoItem = itemContext.addItem(payload)
////   println(newItem)
////    payload.is_flagged = false
////    payload.item_title = "Updating item"
////    payload.is_complete = true
////    println(payload)
////    var updateItem: TodoItem = itemContext.editItem(newItem.item_id, payload)
////    println(updateItem)
//
//    // println(itemContext.deleteItem(21))
//var list: TodoList = itemContext.addListToItem(newItem.item_id, 1)
//   // println(list)
//    //println(dateFormat.format(Date()))
//println("filtered list")
//    var items = itemContext.filterByEndDate(1, "2022-11-28")
//    if (items != null) {
//        for(item in items) {
//            println(item)
//        }
//    }


//    /** Authentication Context **/
//    var auth: AuthenticationContext = AuthenticationContext()
//    var payload: LoginPayload = LoginPayload("maryam@gmail.com", "veronica")
//    var payloadReg: RegistrationPayload = RegistrationPayload("preeti2343", "preeti@gmail.com", "preeti", "Preeti",
//    "Valunjkar", "student")
//    println(auth.login(payload))
//    // println(auth.registration(payloadReg))
//    println(auth.getUser())
//    println(auth.getGG())
//    // println(auth.logout())



    println("==============================================================\n" +
            "                   Welcome to Whatoodo?                        \n" +
            "================================================================\n")
    if (authenticationHandler.isLoggedin()) {
        println("==============================================================\n" +
                "                   Welcome back!                        \n" +
                "================================================================\n")
        println("Access the menu guide for commands using [?menu]")
    } else {

        loginOrRegister()
    }

    commands()




}

fun commands(){
    var command:String = "";
    println("==============================================================\n" +
            "                   Welcome back!                        \n" +
            "================================================================\n")
    println("Access the menu guide for commands using [?menu]")

    while(true) {
        print("whatoodo [?menu for menu options]:")
        command = readln().toString();
        if (command == "quit") {
            break;
        }
        if (command == "logout") {
            authenticationHandler.logout()
            break;
        }
        when(command) {
            "?menu" -> menuOptions()
            "?user" -> userMenu()
            "?item" -> itemMenu()
            "?list" -> listMenu()
            // user menu
            "user-details" ->  userHandler.userDetails(userid)
            "reset-password" -> {
                userHandler.resetPassword(user)
                user = userContext.getUserbyId(userid)!!
            }
            "change-username" -> {
                userHandler.changeUsername(user)
                user = userContext.getUserbyId(userid)!!
            }
            "change-firstname" -> {
                userHandler.changeFirstname(user)
                user = userContext.getUserbyId(userid)!!
            }
            "change-lastname" -> {
                userHandler.changeLastName(user)
                user = userContext.getUserbyId(userid)!!
            }
            "change-occupation" -> {
                userHandler.changeOccuptation(user)
                user = userContext.getUserbyId(userid)!!
            }
            "delete-user" -> {
                userHandler.deleteUser(userid)
                break;
            }
            "current-list" -> {
                if (currentListId == 0L) {
                    println("No current list selected. Use [switch-list] to set a current list.")
                } else {
                    currentListId = listHandler.currentList(currentListId)
                }

            }
            "add-list" -> {
                var userid = listHandler.addList(userid)
                user = userContext.getUserbyId(userid)!!
                authenticationHandler.user = user
            }
            "remove-list" -> {
                var delete: Long = listHandler.removeList()
                println("List with id: $delete was successfully delete")
                var user = userContext.getUserbyId(userid)
                if (user != null) {
                    authenticationHandler.user = user
                }
            }
            "change-list-name" -> {
                if (currentListId == 0L) {
                    println("Switch to a list that exists and try again.")
                } else {
                    var id = listHandler.changeListname(currentListId).list_id
                    currentListId = id
                }

            }
            "change-list-category" -> {
                if (currentListId == 0L) {
                    println("Switch to a list that exists and try again.")
                } else {
                    var id = listHandler.changeCategory(currentListId).list_id
                    currentListId = id
                }

            }
            "favourite-list" -> {
                if (currentListId == 0L) {
                    println("Switch to a list that exists and try again.")
                } else {
                    var id = listHandler.favouriteList(currentListId).list_id
                    currentListId = id
                }

            }
            "copy-list" -> {
                copyList = listHandler.copyList(userid)
                //println(copyList)
            }
            "paste-list"-> {
                listHandler.pasteList(userid, copyList)
                user = userContext.getUserbyId(userid)!!
                if (user != null) {
                    authenticationHandler.user = user
                }
            }
            "duplicate-list" -> {
                listHandler.duplicateList(userid)
                user = userContext.getUserbyId(userid)!!
                if (user != null) {
                    authenticationHandler.user = user
                }
            }
            "switch-list" -> {
                currentListId = listHandler.switchList(currentListId)
                user = userContext.getUserbyId(userid)!!
                if (user != null) {
                    authenticationHandler.user = user
                }
            }
            "add-item" -> {
                if (currentListId == 0L) {
                    println("No current list selected. Use [switch-list] to set a current list.")
                } else {
                    itemHandler.addItem(currentListId)
                    user = userContext.getUserbyId(userid)!!
                }

            }
            "edit-item"-> {
                if (currentListId == 0L) {
                    println("No current list selected. Use [switch-list] to set a current list.")
                } else {
                    itemHandler.editItem()
                    user = userContext.getUserbyId(userid)!!
                    if (user != null) {
                        authenticationHandler.user = user
                    }
                }
            }
            "remove-item" -> {
                if (currentListId == 0L) {
                    println("No current list selected. Use [switch-list] to set a current list.")
                } else {
                    itemHandler.removeItem()
                    user = userContext.getUserbyId(userid)!!
                    if (user != null) {
                        authenticationHandler.user = user
                    }
                }

            }
            "duplicate-item" -> {
                if (currentListId == 0L) {
                    println("No current list selected. Use [switch-list] to set a current list.")
                } else {
                    itemHandler.duplicateItem(currentListId)
                    user = userContext.getUserbyId(userid)!!
                    if (user != null) {
                        authenticationHandler.user = user
                    }
                }

            }
            "copy-item" -> {
                if (currentListId == 0L) {
                    println("No current list selected. Use [switch-list] to set a current list.")
                } else {
                    copyItem = itemHandler.copyItem()!!
                }

            }
            "paste-item" -> {
                if (currentListId == 0L) {
                    println("No current list selected. Use [switch-list] to set a current list.")
                } else {
                    if (copyItem == null) {
                        println("Copy an item to paste. Use the [copy-item] command.")
                    } else {
                        itemHandler.pasteItem(copyItem, currentListId)
                        user = userContext.getUserbyId(userid)!!
                        if (user != null) {
                            authenticationHandler.user = user
                        }
                    }
                }

            }
            "flag-item" -> {
                if (currentListId == 0L) {
                    println("No current list selected. Use [switch-list] to set a current list.")
                } else {
                    itemHandler.flagItem()
                    user = userContext.getUserbyId(userid)!!
                    if (user != null) {
                        authenticationHandler.user = user
                    }
                }

            }
            "show-all-lists" -> {
                user = userContext.getUserbyId(userid)!!
//                println(user.todo_lists)
                for(list in user.todo_lists) {
                    println(list)
                }
            }


            /* list logic, temporary */
//

            "?show" -> showMenu()
        }
    }
    println("Thank you for using whatoodo? Don't forget to set-up reminders to always be on task!")

}

fun loginOrRegister() {
    var command:String = "";
    println("Please login or register to continue.")
    println("Already have an account? Login [login]               ")
    println("Don't have an account? Register [register]                 ")
    while(!authenticationHandler.isLoggedin()) {
        print("whatoodo [?menu for menu options]:")
        command = readln().toString();
        if (command == "quit") {
            break;
        }
        if (command == "logout") {
            authenticationHandler.logout()
            break;
        }
        when(command) {
            "login" -> {
                user = authenticationHandler.login()
                userid = authenticationHandler.user.userid

            }

            "register" -> {
                user = authenticationHandler.register()
                userid = user.userid
            }
        }
    }
}

fun menuOptions() {
    println("whatoodo Menu:");
    println("[?user]    User Menu")
    println("[?list]    Todo List Menu")
    println("[?item]    Todo Item Menu")
    println("[?show]    Show Menu\n")

    println("whatoodo Commands:")
    println("[logout]   Logout")
    println("[quit]     Quit\n")
}

fun userMenu() {
    println("User Menu Commands:")
    println("[user-details]         Show all user details")
    println("[reset-password]       Reset Password")
    println("[change-username]      Change Username")
    println("[change-firstname]     Change Firstname")
    println("[change-lastname]      Change Lastname")
    println("[change-occupation]    Change Occupation")
    println("[delete-user]          Delete User")
    /** Add more commands below as required **/
}

fun listMenu() {
    println("Todo List commands:")
    println("[current-list]             Displays current list")
    println("[add-list]                 Add a new list")
    println("[remove-list]              Remove a list")
    println("[change-list-name]         Modify the list name")
    println("[change-list-category]     Modify the list category")
    println("[favourite-list]           Favourite or un-favourite the list")
    println("[copy-list]                Copy current list")
    println("[paste-list]               Pastes a list into current list")
    println("[duplicate-list]           Duplicates current list")
    println("[switch-list]              Changes current list into an another existing list")

    /** Add more commands as required **/
    /** for sorting **/
}

fun itemMenu() {
    println("Todo Item commands:")
    println("[add-item]             Add new item to current list")
    println("[edit-item]            Edit item on current list")
    println("[remove-item]          Remove item from current list")
    println("[duplicate-item]       Duplicate item in current list")
    println("[copy-item]            Copy item from current list")
    println("[paste-item]           Paste item into current list")
    println("[flag-item]            Flag item in current list")
//    println("[undo-item]            Undo action on item")
//    println("[redo-item]            Redo action on item")

    /** Add more commands as required **/


}

fun showMenu() {
    println("Show ToDo List & ToDo Item commands:")
    println("[show-all]             Displays all lists")
    println("[show-list]            Displays current list")
    println("[show-total-list]      Displays total number of lists")
    println("[show-total-items]     Displays total number of items in current list")
    println("[show-item]            Displays item on current list")
    println("[show-user]            Displays all user details")

    /** Add more commands as needed - shared list, export lists, etc. **/

}
